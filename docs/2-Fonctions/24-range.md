# 🏬 La fonction `range`

La fonction `range` est très utilisée dans les boucles, elle renvoie **un à un**, des termes d'une suite arithmétique.

On peut découvrir ici la fonction native `range` sans écrire de boucle.

Pour cela, on va convertir le résultat de `range` en `tuple`.

!!! danger "Point technique"
    :warning: Dans la version 2 de Python, `range` fabriquait une liste. Ce n'est plus le cas.

    En effet, très souvent, on souhaite utiliser des éléments, les uns après les autres, sans forcément
    avoir besoin de conserver la liste. On souhaite avoir chacun son tour des éléments.

    Dans la version 3 de Python, `range` renvoie, un à un, un élément d'une suite arithmétique.

    Pour construire le résultat global, ici, on va les assembler dans un `tuple`. Uniquement dans un but pédagogique, pour comprendre le fonctionnement de `range`.

## Plusieurs paramètres possibles

### `range` à un paramètre

- `range(n)` renvoie les entiers de $0$ **inclus** à $n$ **exclu**.

```pycon
>>> tuple(range(5))
(0, 1, 2, 3, 4)
>>>
```

### `range` à deux paramètres

- `range(n, m)` renvoie les entiers de $n$ **inclus** à $m$ **exclu**.

```pycon
>>> tuple(range(5, 10))
(5, 6, 7, 8, 9)
>>>
```

:warning: **Rappel**, pour un `tuple` à un élément, il y a une virgule après l'élément. Cela permet de différencier `(5)` qui est un entier, de `(5,)` qui est un tuple à un élément. Techniquement, pour tout `tuple` **non vide**, on peut l'écrire avec une virgule finale superfétatoire comme `(5, 6, 7) = (5, 6, 7,)`.

```pycon
>>> tuple(range(5, 6))
(5,)
>>>
```

:warning: Si $n\geqslant m$, rien n'est renvoyé.

```pycon
>>> tuple(range(8, 8))
()
>>>
```

### `range` à trois paramètres

- `range(n, m, p)` renvoie les entiers de $n$ **inclus** à $m$ **exclu**, par pas de $p$.

```pycon
>>> tuple(range(5, 10, 2))
(5, 7, 9)
>>> tuple(range(10, 5, -1))
(10, 9, 8, 7, 6)
>>>
```

## Exercices

On rappelle que `#!python assert` permet de vérifier une condition écrite juste après.

- Si le test est valide, alors le programme continue sans message d'erreur.
- Si le test est invalide, alors le programme s'arrête et vous avez un message d'erreur.

Votre objectif est d'exécuter les cellules sans aucune erreur.

{{ python_carnet('scripts/range.ipynb') }}
