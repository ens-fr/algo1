# 📐 Le module math

Le module [^doc] `math` (à écrire sans `s`) propose des fonctions utiles en mathématiques.

[^doc]: :fontawesome-brands-python: La [documentation sur le module `math`](https://docs.python.org/fr/3/library/math.html?highlight=module%20math#module-math){ target=_blank } nous propose encore d'autres fonctions.


=== "Bonne pratique"
    Vous avez besoin des fonctions `sqrt`, `factorial`, `gcd` et `comb` ? Alors n'importez que celles-ci !

    ```pycon
    >>> from math import sqrt, factorial, gcd, comb
    >>> factorial(5)
    120
    >>> sqrt(169)
    13.0
    >>> gcd(12, 16)
    4
    >>> comb(5, 2)
    10
    ```

=== "Autre pratique"
    On peut aussi faire `#!python import math`, dans ce cas, les constantes et fonctions se font appeler en préfixant par `math.`

    ```pycon
    >>> import math
    >>> math.factorial(5)
    120
    >>> math.pi
    3.141592653589793
    ```

    L'intérêt est de pouvoir conserver l'utilisation d'une fonction native et/ou d'autres fonctions d'autres modules qui auraient le même identifiant !

=== "Pratique discutable"
    On peut calculer les dix derniers chiffres du plus grand nombre premier [^pgp] à la fin de l'année 2018 ainsi :

    [^pgp]: :fontawesome-brands-wikipedia-w: [Le plus grand nombre premier connu](https://fr.wikipedia.org/wiki/Plus_grand_nombre_premier_connu){ target=_blank } a varié au court du temps, avec un changement radical depuis l'arrivée des ordinateurs.

    ```pycon
    >>> pow(2, 82_589_933, 10_000_000_000) - 1
    5217902591
    >>> from math import *
    >>> pow(2, 82_589_933, 10_000_000_000) - 1
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: pow expected 2 arguments, got 3
    ```

    La seconde tentative a échoué à cause de `#!python from math import *`

    En effet, l'import du module `math` s'est accompagné d'une fonction `math.pow` qui a écrasé la fonction native `pow` différente.

    :warning: On voit très souvent `#!python from math import *`, c'est pratique et rapide à écrire. Mais cela causera des erreurs en maths expertes.

!!! tip "Conseils"
    
    1. :warning: On n'importe que ce dont on a besoin !
    2. Un seul import suffit pour l'ensemble d'un script, ou d'une session console.

## `sqrt` ; racine carrée

Pour _**sq**uare **r**oo**t**_ ; la racine carrée, le résultat est toujours un flottant.

```pycon
>>> sqrt(49)
7.0
>>> sqrt(1e300)
1e+150
>>> sqrt(1e-300)
1e-150
>>> sqrt(-5)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: math domain error
>>>
```

!!! example "Explications"
    1. $\sqrt{49} = 7$
    2. $\sqrt{1×10^{300}} = 1×10^{150}$
    3. $\sqrt{1×10^{-300}} = 1×10^{-150}$
    4. $\sqrt{-5}$ provoque une erreur, $-5$ n'est pas dans le domaine de définition.

:warning: Pensez à lire les messages d'erreurs ; ils sont souvent très explicites. N'ayez pas peur !

## `ceil` ; partie entière par excès

`#!python ceil(x)` renvoie la partie entière par excès de `x`.

Pour _Ceil_ ; plafond en anglais, le résultat est toujours un entier.

!!! faq "Exercice"
    Chaque bus a une capacité de $45$ places.

    Combien faut-il de bus pour transporter $587$ personnes ?

```pycon
>>> from math import ceil
>>> effectif = 587
>>> capacité = 45
>>> nb_bus = ceil(effectif / capacité)
>>> nb_bus
14
```

## `floor` ; partie entière par défaut

`#!python floor(x)` renvoie la partie entière par défaut de `x`.

Pour _Floor_ ; sol en anglais, le résultat est toujours un entier.

!!! faq "Exercice"
    Chaque bouteille a un volume de $0.75~\text{l}$.

    Combien peut-on obtenir de bouteille pleine avec une production de $5870~\text{l}$ ?

```pycon
>>> from math import floor
>>> production = 5870     # en L
>>> unité = 0.75          # en L
>>> nb_bouteilles = floor(production / unité)
>>> nb_bouteilles
7826
```

!!! warning "Différence entre `int` et `floor`"
    Les fonctions `int` et `floor` coïncident sur $\mathbb R^+$, mais pas sur $\mathbb R^-$ !

    ```pycon
    >>> int(7.5)
    7
    >>> floor(7.5)
    7
    >>> int(-7.5)
    -7
    >>> floor(-7.5)
    -8
    ```

## `exp` ; exponentielle

`#!python exp(x)` renvoie $e$ à la puissance `x`, où $e = 2.718281…$ est la base des logarithmes naturels.

C'est en général plus précis que `#!python math.e ** x` ou `#!python pow(math.e, x)`.

## Fonctions trigonométriques

`cos, acos, sin, asin, tan, atan` utilisent les angles en radians.
