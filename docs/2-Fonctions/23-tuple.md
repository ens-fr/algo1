# 🎏 Doublet, n-uplet : `tuple`

On peut faire pointer deux (ou plusieurs) nombres par une variable.

On peut aussi extraire ses composantes :

```pycon
>>> point_A = (5, 7)
>>> pA_x, pA_y = point_A
>>> pA_x
5
>>> pA_y
7
>>> type(point_A)
<class 'tuple'>
>>> type(pA_x)
<class 'int'>
>>>
```

!!! note "Explications"
    1. On a créé un `#!python tuple` avec deux valeurs,
        - séparées par une virgule,
        - entourées de parenthèses.
    2. On extrait (_unpack_) les valeurs, qui sont pointées par `pA_x` et `pA_y`.
    3. On a affiché les composantes ; on aurait pu faire des calculs avec.
    4. Le type Python pour les doublets, triplets et autres n-uplets est `tuple`.

!!! danger "`tuple` à un élément"
    On peut techniquement créer un `tuple` à une seule composante, on ne le recommande pas pour le risque de confusion possible.
    
    ```pycon
    >>> singleton = (5,)
    >>> type(singleton)
    <class 'tuple'>
    >>> singleton == 5
    False
    ```

## Exercice

On donne 4 points : `point_A`, `point_B`, `point_C`, `point_D`, la question est de savoir si $ABCD$ forme un parallélogramme. Pour cela on sait qu'il suffit de vérifier que $[AC]$ et $[BD]$ ont le même milieu.

Compléter le programme en ce sens.

{{ IDEv('parallélogramme') }}

!!! tip "Conseil"
    On essayera de faire une version sans faire **aucune** division, même pas par deux.

    L'exemple donné est bien un parallélogramme.

    Trouver d'autres exemples qui sont ou non des parallélogrammes, et vérifier.
