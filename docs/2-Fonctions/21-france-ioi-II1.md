# 👾 France-IOI (II-1)


Le niveau 2 de France-IOI commence avec un travail sur les flottants. [Nombres à virgules et autres outils](http://www.france-ioi.org/algo/chapter.php?idChapter=650){target=_blank}

 Voici un peu d'aide pour commencer.

## 1) Origami

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=1890) est :

> L'épaisseur d'une feuille de papier est de 110 micromètres, c'est-à-dire 0,110 millimètre. Si on la plie 15 fois sur elle-même et que l'épaisseur double à chaque fois, quelle sera l'épaisseur finale si on l'exprime en centimètres ? Votre programme devra calculer et afficher cette valeur (qui n'est pas forcément entière).

??? tip "Indice 1"
    Convertir immédiatement l'épaisseur en centimètre.

??? tip "Indice 2"
    On pourra modifier et compléter ce code

    ```python
    epaisseur = ...  # en centimètre
    REPÉTER 15 FOIS
        epaisseur = epaisseur * 2
    print(epaisseur)
    ```

## 2) Conversions de distances

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=1924) est :

>  Écrivez un programme qui lit un nombre décimal (un nombre à virgule) représentant un nombre de lieues et affiche le nombre de kilomètres correspondant. Un kilomètre vaut exactement 0.707 lieue.

??? tip "Indice 1"
    Ajouter les unités en commentaires facilite l'analyse dimensionnelle.

    Il est plus facile de trouver la bonne opération.

??? tip "Indice 2"
    ```python
    un_km_en_lieue = 0.707           # en km / lieue
    distance_lieue = float(input())  # en lieue
    distance_km = ...                # en km
    print(distance_km)
    ```

## 3) Comparatif de prix

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=1936) est :

>  Votre programme doit d'abord lire le nombre de légumes mis en vente. Ensuite, pour chacun, il doit lire 3 nombres décimaux : son poids, son âge (en nombre de jours depuis la cueillette), et son prix de vente. Votre programme doit ensuite afficher pour chaque légume son prix au kg (au fur et à mesure que les légumes sont présentés).

??? tip "Indice 1"
    Penser aux unités en commentaires

??? tip "Indice 2"

    ```python
    nb_legumes = int(input())
    REPÉTER nb_legumes FOIS
        poids = float(input())   # en kg
        age = float(input())     # en jours
        prix = float(input())    # en €
        prix_kilo = ...          # en €/kg
        print(prix_kilo)
    ```

## 4) Moyenne des notes

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=1935) est :

> Votre programme doit d'abord lire un premier entier, qui décrit le nombre de notes obtenues. Ensuite, il doit lire chacune de ces notes, qui sont également des nombres entiers. Enfin, il doit afficher la moyenne de toutes ces notes.

??? tip "Indice 1"

    ```python
    nb_notes = int(input())
    ...
    REPÉTER nb_notes FOIS
        note = int(input())
        ...
    moyenne = ...
    print(moyenne)
    ```

??? tip "Indice 2"
    Il sera utile de connaitre `somme_notes` pour le calcul de la moyenne.

    Il faudra bien initialiser cette variable ; idéalement avec le type `float`.

## A1) Augmentation de la population

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=1706) est :

>  Votre programme devra lire un entier, la population actuelle de la ville, puis un nombre décimal, la croissance prévue de la population, en pourcentage. Il devra alors afficher la nouvelle population de la ville sous la forme d'un nombre entier. On considèrera, par convention, qu'une population de 31,4 habitants signifie qu'il y a 31 habitants, on ne compte donc que les habitants « entiers » !

??? tip "Indice 1"
    Compléter le code

    ```python
    population_actuelle = int(input())
    taux_croissance = float(input())
    population_future = ...
    ```

??? tip "Indice 2"
    Le coefficient multiplicatif associé à une augmentation de $t~\%$ est $\left(1 + \dfrac{t}{100}\right)$

??? tip "Indice 3"
    La population future doit être entière, et c'est une valeur par défaut qu'il faut prendre ; pas un arrondi. On utilise donc `floor` du module `math`.


## A2) Construction de maisons

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&iOrder=17) est :

> Votre programme devra lire un nombre décimal, la quantité de ciment nécessaire pour les fondations de votre nouvelle maison, en kilos. Sachant que le ciment n'est vendu qu'en sacs de 60 kilos et qu'un sac coute 45 euros, votre programme devra afficher le cout total du ciment.

??? tip "Indice 1"
    Penser aux unités en commentaire.

??? tip "Indice 2"
    Compléter le code

    ```python
    masse_ciment = float(input())   # en kg
    MASSE_1_SAC =  60               # en kg
    PRIX_1_SAC = 45                 # en €
    nb_sacs = ...
    prix_sacs = ...
    print(prix_sacs)
    ```

## B1) Soirée orageuse

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=2285) est :

> Votre programme devra lire un décimal, le temps (en secondes) entre le moment où vous voyez l'éclair et le moment où vous entendez le tonnerre. Il devra calculer et afficher la distance entre vous et l'orage, arrondi au kilomètre près.
>
> On supposera que la lumière se déplace instantanément. La vitesse du son dépend de paramètres comme l'altitude, la température... mais on supposera qu'en cette soirée elle vaut 340,29 mètres / seconde. 

??? tip "Indice 1"
    Penser aux unités en commentaire.

??? tip "Indice 2"
    Compléter le code

    ```python
    duree_eclair = float(input())     # en s
    VITESSE_SON = 340.29              # en m/s
    distance_m = ...                  # en m
    distance_km = ...                 # en km
    print(...(distance_km))  # arrondie à l'unité
    ```

## B2) Augmentation des taxes

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=1996) est :

>  Votre programme doit lire trois nombres décimaux : la valeur actuelle de la taxe sur les fruits et légumes (en pourcentage), la nouvelle valeur de la taxe (en pourcentage), puis le prix actuel d'un légume, taxes comprises, en euros. Il devra calculer et afficher le prix du légume avec la nouvelle valeur de la taxe, arrondi au centime près.

??? tip "Indice 1"
    Compléter le code

    ```python
    taxe_actuelle = float(input())        # en %
    taxe_future = float(input())          # en %
    prix_actuel = float(input())          # en €
    ...
    prix_futur = ...                      # en €
    ###### deux possibilités ######
    # première, avec arrondi du nombre entier de centimes
    prix_futur_c = ...                  # en centimes
    print(round(prix_futur_c) / ...)    # en €

    # deuxième, avec round à deux paramètres
    print(round(prix_futur, ...))    # en €
    ```

??? tip "Indice 2"
    Le coefficient multiplicatif associé à une augmentation de $t~\%$ est $\left(1 + \dfrac{t}{100}\right)$

??? tip "Indice 3"
    Pour retrouver un prix hors taxe, on prend le prix avec taxe, et on le **divise** par le coefficient multiplicatif.

## C1) Achat de livres

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&iOrder=23) est :

> Votre programme doit commencer par lire la somme d'argent dont vous disposez et lira ensuite le prix d'un livre. Il devra ensuite afficher un entier, le plus grand nombre de livres qu'il vous est possible d'acheter avec cette somme d'argent.

??? tip "Indice 1"
    Compléter le code

    ```python
    somme_argent = int(input())     # en €
    prix_1_livre = int(input())     # en €
    nb_livres = ...
    print(nb_livres)
    ```

??? tip "Indice 2"
    Une seule barre de division produit un résultat flottant ; on veut un résultat entier.

## C2) Une belle récolte

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&iOrder=24) est :

> Votre programme doit commencer par lire un entier _**nb_personnes**_ puis un entier _**nb_fruits**_. Il doit ensuite afficher `"oui"` si _**nb_fruits**_ est un multiple de _**nb_personnes**_, et `"non"` dans le cas contraire. 

??? tip "Indice 1"
    Modifier et compléter le code

    ```python
    nb_personnes = int(input())
    nb_fruits = int(input())
    SI nb_fruits EST MULTIPLE DE nb_personnes
        print("oui")
    SINON
        print("non")
    ```

??? tip "Indice 2"
    Être multiple est en lien avec être divisible.

    On pourra vérifier si un reste dans une division entière est égal à zéro.

## C3) La roue de la fortune

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=650&idTask=2463) est :

>  Votre programme doit commencer par lire un entier _**nb_zones**_. Sachant que la roue va tourner de _**nb_zones**_ zones, vous devez calculer (puis afficher) sur quelle zone (parmi les 24) le curseur va arriver.
>
> Ainsi, si la route tourne de +2 zones alors le curseur arrive sur la zone 2, et si la roue tourne de -2 zones, alors le curseur arrive sur la zone 22.

??? tip "Indice 1"
    Compléter le code

    ```python
    nb_zones = int(input())
    zone_finale = ...
    print(zone_finale)
    ```

??? tip "Indice 2"
    Après avoir fait plusieurs tours (des paquets de 24 zones), on aimerait savoir combien il **reste** de zones à faire tourner.

!!! info "Facile avec Python"
    Ce problème est plus simple avec Python qu'avec d'autres langages de programmation. En effet, le comportement pour un nombre négatif est le même en maths qu'avec Python, mais pas avec d'autres langages...
