def est_divible(n, d

Renvoie True  si n est divisible par d
        False sinon
... # À corriger et compléter








#-----------------------------------
# TESTS ; appuyer sur Exécuter
# Ne pas modifier ci-dessous
#-----------------------------------

assert est_divible(15, 3) == True
assert est_divible(15, 7) == False
assert est_divible(21, 7) == True
assert est_divible(21, 8) == False
print("La fonction `est_divisible` semble correcte.")
