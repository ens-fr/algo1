# 📖 Documenter ses fonctions

## _docstring_ de fonction

Une _docstring_ est une chaine de caractère qui fournit de l'aide
sur la fonction.

- Elle n'est pas obligatoire, mais **fortement conseillée**.

On recommande d'en placer à toute fonction quand son nom n'est pas totalement explicite.

1. Soit la _docstring_ est courte, elle est sur une seule ligne et est encadrée entre `"`.
2. Soit la _docstring_ est sur plusieurs lignes
    - On commence par `"""`,
    - chaque ligne sera indentée normalement,
    - et on finit par `"""` seul sur sa ligne, indenté comme le premier.
3. Voici des exemples

```python
def comb_2(n):
    "Renvoie le nombre de façon de choisir 2 éléments parmi n"
    return n * (n-1) // 2

def perimetre_rectangle(base, hauteur):
    return 2*(base + hauteur)
    # ici la docstring était inutile...

def somme_chiffres(n):
    """Renvoie la somme des chiffres de l'entier n

    >>> somme_chiffres(1337)
    14

    """
    return ... # à compléter
```

La dernière fonction montre une _docstring_ sur plusieurs lignes qui
comportent un exemple d'utilisation ; c'est une excellente pratique.

On recommande cette pratique.

### Utilisation des _docstring_

1. Dans une console si on entre `>>> help(ma_jolie_fonction)` on obtient de l'aide : sa _docstring_.
Construisez vos _docstring_ comme une aide à un utilisateur qui la découvre. Ça peut être vous dans quelques mois.
2. Un éditeur moderne affiche l'aide automatiquement grâce à la _docstring_.

## Exercices sur _docstring_

### Corriger un code

Pour les codes suivants, trouver les erreurs :

#### Question 1

=== "À corriger"

    ```python
    def discriminant(a, b, c):
        """Renvoie le discriminant du polynôme P avec
        P(x) = ax² + bx + c   """
        return b*b - 4*a*c
    ```

=== "Réponse"

    ```python
    def discriminant(a, b, c):
        """Renvoie le discriminant du polynôme P avec
        P(x) = ax² + bx + c
        """
        return b*b - 4*a*c
    ```

    :warning: La _docstring_ doit se finir par `"""` seul sur la ligne.

    Astuce 1 : `b*b` est plus rapide à calculer que `b**2`

    Astuce 2 : Les opérations prioritaires peuvent rester collées si elles
    n'ont qu'un caractère par opérande, cela reste lisible.

#### Question 2

=== "À corriger"

    ```python
    def évaluation(a, b, c, x):
        "Renvoie l'évaluation du polynôme P en x avec
        P(x) = ax² + bx + c
        "
        return (a*x + b)*x + c
    ```

=== "Réponse"

    ```python
    def évaluation(a, b, c, x):
        """Renvoie l'évaluation du polynôme P en x avec
        P(x) = ax² + bx + c
        """
        return (a*x + b)*x + c
    ```

    :warning: La _docstring_ sur plusieurs lignes doit commencer et se finir par `"""`.

#### Question 3

=== "À corriger"

    ```python
    def f(n):
        "Renvoie le triple de "n", plus un"
        return 3*n + 1
    ```

=== "Réponse"
    Deux solutions, parmi d'autres

    ```python
    def f(n):
        "Renvoie le triple de 'n', plus un"
        return 3*n + 1
    ```

    ou mieux

    ```python
    def f(n):
        "Renvoie le triple de `n`, plus un"
        return 3*n + 1
    ```

    :warning: La _docstring_ sur une ligne qui commence et se finit par `"` ne peut pas contenir d'autre `"` sans les échapper.

## Création de fonction

### Exercice : `est_divisible`

Corriger et compléter la fonction suivante.

{{ python_ide('scripts/fonctions_1.py', hauteur=700) }}

??? done "Solution"
    === "Solution verbeuse"

        ```python
        def est_divible(n, d):
            """Renvoie True  si n est divisible par d
                    False sinon
            """
            if 0 == n % d:
                return True
            else:
                return False
        ```

    === "Solution élégante"

        ```python
        def est_divible(n, d):
            """Renvoie True  si n est divisible par d
                    False sinon
            """
            return 0 == n % d
        ```

    Choisissez la solution qui vous convient le mieux.

!!! info "Utiliser `assert` : une bonne pratique"
    Quand on crée une fonction, on doit la tester un minimum.

    La bonne première méthode est d'écrire quelques vérités à vérifier.

    On utilise alors `assert <vérité>, "<message en cas d'erreur>"`

    - Si le test `<vérité>` est vérifié, le programme continue silencieusement.
    - Si le test `<vérité>` échoue, le programme s'arrête avec le message d'erreur préparé.

    Pour les utilisateurs avancés, on fabrique des jeux de tests plus poussés et automatisés.

    - C'est une partie de l'évaluation en algorithmique.
    - Savoir fabriquer des jeux de tests robustes est une compétence recherchée.

### Exercice : Quotient par excès

Créer une fonction qui renvoie le nombre de bus nécessaires au transport de $n$ personnes.

Chaque bus a une capacité maximale de $d$ places.

On travaillera avec $d>0$

```python
def quotient_excès(n, d):
    # à compléter
    ...
```

Tests
```pycon
>>> quotient_excès(100, 45)
3
>>> quotient_excès(90, 45)
2
```

??? done "Indications"
    1. On peut trouver une solution avec des décalages de $1$.
    2. On peut trouver une solution élégante en travaillant avec $-n$.

    Cette fonction est très utile en pratique !

### Exercice : `somme_des_chiffres`

En s'inspirant d'une fonction `mystère` vue précédemment,
proposer une fonction `somme_des_chiffres` qui devra passer les tests suivants :

```python
assert somme_des_chiffres(1337) == 14
assert somme_des_chiffres(5) == 5
assert somme_des_chiffres(10) == 1
assert somme_des_chiffres(0) == 0
```

Votre fonction devra avoir une _docstring_.

Les tests seront inclus dans le script après la fonction.

1. Utiliser une [console Basthon](https://console.basthon.fr/){ target=_blank }, et une fois réussi,
2. Cliquer sur :fontawesome-solid-share-alt-square:, puis « Copier dans le presse-papier »
3. Partager votre solution en communiquant ce lien.
