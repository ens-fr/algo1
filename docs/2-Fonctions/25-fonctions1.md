# 🪄 Création de Fonction

Observons d'abord quelques utilisations de fonctions créées.

## Quelques exemples

=== "Polynome"

    ```python
    def f(n):
        return n * (n+1) // 2
    ```

    ```pycon
    >>> f(5)
    15
    ```

=== "_Culture geek_"

    ```python
    def réponse_question_universelle(): return 42
    ```

    ```pycon
    >>> réponse_question_universelle()
    42
    ```


=== "Géométrie"

    ```python
    def périmètre_rectangle(base, hauteur):
        return 2*(base + hauteur)
    ```

    ```pycon
    >>> périmètre_rectangle(base=7, hauteur=3)
    20
    ```

=== "Problème de Syracuse"

    ```python
    def collatz(n):
        if n % 2 == 0:
            suivant = n // 2
        else:
            suivant = 3*n + 1
        return suivant
    ```

    ```pycon
    >>> collatz(7)
    22
    ```

=== "Récursivité"

    ```python
    def mystère(entier):
        if entier < 0:
            return mystère(-entier)
        elif entier >= 10:
            return mystère(entier // 10)
        else:
            return entier
    ```

    ```pycon
    >>> mystère(-42 ** 1337)
    1
    ```

=== "Arithmétique"

    ```python
    def est_divisible(n, d):
        if d == 0:
            raise ValueError("On ne divise pas par zéro ; merci.")
        return n % d == 0
        # cette fonction est à retenir !!!
    ```

    ```pycon
    >>> est_divisible(15, 3)
    True
    ```

!!! cite "Premières définitions de fonctions"
    1. On commence par le mot clé `#!python def`
    2. On donne un **nom de variable** à la fonction
    3. On donne les paramètres : zéro, un ou plusieurs ; entre parenthèses, séparés par des virgules
    4. On termine cette ligne par `:`
    5. On donne le corps de la fonction (sur la même ligne) ou (mieux)
     sur plusieurs lignes ; dans ce cas, c'est un bloc indenté.
    6. Remarque : indenté comme indentation ; la technique de Python pour délimiter les blocs.
    6. Il peut y avoir un ou plusieurs `#!python return`.
        - Si la fonction arrive à la fin du bloc un `#!python return None` est automatiquement ajouté.
        - Une **fonction renvoie toujours** une valeur, avec `#!python return`.
    7. Une fonction peut appeler une autre fonction ; voire s'appeler elle-même (récursivité).
    8. Une fonction peut utiliser d'autres constructions Python
        - `mystère` et `collatz` utilisent
          des structures conditionnelles `#!python if elif else` que nous avons déjà vu.
        - `est_divisible` utilise un mécanisme pour provoquer volontairement une erreur
          contrôlée. Ici, en cas de tentative de division par zéro, on envoie un message.
          On peut alors retrouver le cheminement d'appels de fonctions qui
          a conduit à l'arrêt du programme. Nous verrons cela bien plus tard,
          mais c'est une très bonne pratique.


```python
def nom_explicite_de_la_fonction(parametre1, parametre2, ...):
    #####################################
    #                                   #
    corps dela fonction                 #
    #                                   #
                    return ...          #
            return ...                  #
    #####################################
    return ...
```


!!! tip "Pour utiliser une fonction"
    **Dans un premier temps**, on conseille

    1. D'écrire la fonction dans un script, avec [Thonny](https://thonny.org/){ target=_blank }, 
    [Basthon (console ou carnet)](https://basthon.fr/){ target=_blank } ou [NumWorks](https://www.numworks.com/simulator/){ target=_blank }, ou directement ici avec Pyodide.
    2. :fontawesome-solid-cogs: Exécuter ce script.
    3. Appeler la fonction dans la console.

!!! abstract "Bonnes pratiques sur les noms de variables"

    1. Contrairement aux mathématiques, avec Python :
        - il est facile de donner des noms de variables à plusieurs lettres et
        - il est difficile de donner des noms de variables avec une seule lettre comme
            - $\mathfrak p$ en fraktur[^1],
            - $\vec \omega$ en grec,
            - $\mathscr C$ en cursive, ou
            - $\mathcal D$ calligraphiée.
        - On recommande donc d'utiliser des noms de variables à plusieurs lettres, qui donnent du sens, dès que possible.
    2. Les accents sont tolérés pour du code en français, à destination du public français.
        - Pour du code international, l'anglais (sans accent donc) est de rigueur. De même que pour d'autres langages de programmation. Il vaut mieux éviter les accents.
    3. On peut utiliser les noms des paramètres dans l'appel de
     la fonction,
        - comme avec `#!python périmètre_rectangle(hauteur=3, base=7)`.
        - C'est une bonne pratique.
        - Cela permet de changer l'ordre des paramètres, de mieux comprendre l'appel.
        - Pas d'espace autour du signe `=` dans ce cas.
    4. Une autre bonne pratique que nous verrons très bientôt est l'auto-documentation des fonctions.
        - On utilisera une _docstring_.

[^1]: L'alphabet [fraktur](https://fr.wikipedia.org/wiki/Fraktur#Alphabet_Fraktur_en_Unicode_et_utilisation_en_science){ target=_blank }

!!! danger "Défi :boom:"

    ```python
    def mystère(entier):
        if entier < 0:
            return mystère(-entier)
        elif entier >= 10:
            return mystère(entier // 10)
        else:
            return entier
    ```

    Expliquer ce que fait concrètement la fonction `mystère`.

    À quoi sert-elle ? Essayer sur des exemples.

    ??? tip "Indice"
        On peut vérifier que

        1. `mystère(5)` renvoie `5`.
        2. `mystère(-5)` renvoie `5`.
        2. `mystère(42)` renvoie `mystère(4)`, qui renvoie `4`.

        ??? done "Solution"
            La fonction `mystère` renvoie le premier chiffre (le plus significatif) d'un entier.

            Il s'agit d'une fonction récursive, elle s'appelle elle-même. On utilise souvent la récursivité en Terminale.
