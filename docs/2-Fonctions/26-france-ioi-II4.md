# 👾 France-IOI (II-4)


Le niveau 2 de France-IOI poursuit avec un travail sur les [fonctions](http://www.france-ioi.org/algo/chapter.php?idChapter=509){target=_blank}

 Voici un peu d'aide pour commencer.

## 1) Code secret deux fois

> L'objectif de cet exercice est d'utiliser une fonction pour éviter de recopier deux fois les instructions qui permettent d'attendre le code 4242. 

## 2) Deux codes secrets

> Ici, écrivez une et une seule fonction pour demander successivement les deux codes.

## 3) Dentelle

> Votre programme doit lire la longueur de la dentelle, puis l'afficher sous la forme de trois lignes remplies respectivement de `X`, de `#` et de `i`.

=== "Exemple"

    ```email
    5
    ```

    ```python
    def dentelle(...):
        ...

    longueur = int(input())
    dentelle(longueur)
    ```

    ```output
    XXXXX
    #####
    iiiii
    ```

!!! tip "Conseil"
    Une solution élégante utilisera **deux** fonctions. La fonction `dentelle` qui est demandée, mais aussi une fonction `ligne_de` qui sera appelée par `dentelle`, idéalement dans une boucle de trois tours en itérant sur `"X#i".



## 4) Motif rectangulaire

> Afficher un motif rectangulaire

=== "Exemple 1"

    ```email
    4
    9
    F
    ```

    ```python
    def motif_rectangulaire(...):
        ...

    nb_lignes = int(input())
    nb_colonnes = int(input())
    lettre = input()
    motif_rectangulaire(nb_lignes, nb_colonnes, lettre)
    ```

    ```output
    FFFFFFFFF
    FFFFFFFFF
    FFFFFFFFF
    FFFFFFFFF
    ```

=== "Exemple 2"

    ```email
    8
    3
    P
    ```

    ```python
    def motif_rectangulaire(...):
        ...

    nb_lignes = int(input())
    nb_colonnes = int(input())
    lettre = input()
    motif_rectangulaire(nb_lignes, nb_colonnes, lettre)
    ```

    ```output
    PPP
    PPP
    PPP
    PPP
    PPP
    PPP
    PPP
    PPP
    ```

## 5) Le plus petit de deux entiers

> Écrivez une fonction nommée `min2`, qui prend deux entiers en paramètres et renvoie le plus petit. [...]

=== "Exemple"
    
    ```email
    4
    3
    6
    2
    6
    8
    9
    8
    5
    4
    ```

    ```python
    def min2...
        ...
        return ...
        return ...
    
    ...
    for _ in range(10):
        n = int(input())
        ...
    print(minimum)
    ```

    ```output
    2
    ```

## 6) Phénomène numérique

> Terme suivant au problème 3Np1


=== "Exemple"
    
    ```email
    7
    ```

    ```python
    def terme_suivant(...
        ...
    

    n = int(input())
    ...
        ...
        print(n, end=" ")
    print(...)
    ```

    ```output
    22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1
    ```


## 7) Distance euclidienne

> Renvoie la distance euclidienne entre deux points.

=== "Exemple"

    ```email
    22.5
    46.8
    4.25
    7.22
    ```

    ```python
    from math import sqrt

    def distance...
        ...

    x1 = int(input())
    y1 = int(input())
    x2 = int(input())
    y2 = int(input())
    
    print(distance(...))
    ```

    ```output
    43.584847
    ```

!!! tip "Conseils"
    1. L'exemple du sujet proposé sur France-IOI est cassé lors du test. Ignorer ce test et soumettre votre fonction quand elle prête.
    2. Inutile d'arrondir le résultat.
    3. Si vous faîtes un copier-coller de la formule, vous aurez un problème avec le signe `−` qui n'est pas le même caractère que `-` ; et vous aurez une erreur curieuse. Conseil ici : taper la formule au clavier.


## 8) Formes creuses

>  Votre objectif doit être d'obtenir le code source le plus simple et clair possible, en le décomposant en fonctions.

=== "Exemple"

    ```email
    15
    5
    12
    6
    ```

    ```python
    nb_X = int(input())
    nb_lignes_rectangle = int(input())
    nb_colonnes_rectangle = int(input())
    nb_lignes_triangle = int(input())

    def ligne...
        ...
    
    def rectangle...
        ...
    
    def triangle...
        ...

    ligne(...
    print()
    rectangle(
    print()
    triangle(
    ```

    ```output
    XXXXXXXXXXXXXXX

    ############
    #          #
    #          #
    #          #
    ############

    @
    @@
    @ @
    @  @
    @   @
    @@@@@@
    ```

!!! tip "Conseils"
    1. On conseille de créer deux fonctions `ligne`, une fonction `ligne_creuse` et une fonction `ligne_pleine`.
    2. Il faut gérer les cas où la hauteur ou la largeur du rectangle (ou du triangle) vaut $1$. Il faudra faire des tests.
    3. Tester aussi votre programme avec des hauteurs de $2$.


---

:warning: Pour ce dernier exercice, il vaut mieux avoir commencé les sections `Tableau` et `string`.

---


## 9) Convertisseur d'unités

> Affichez en sortie les valeurs converties, suivies d'une espace et de leur unité : `p`, `l` ou `f`.

=== "Exemple"

    ```email
    4
    12.3 m
    1245.243 g
    37.2 c
    23 g
    ```

    ```python
    # Fonctions de conversions, q pour quantité

    def vers_pied(q_metre):
        ...

    def vers_livre(q_gramme):
        ...

    def vers_fahrenheit(q_celcius):
        ...

    nb_conversions = int(input())
    for _ in range(nb_conversions):
        nombre_txt, unite = input().split()  # Attention (1)
        nombre = float(nombre_txt)
        if unite == 'm':
            ...
        elif ...

    ```

    1. La lecture se décompose en
        - `input()` renvoie la ligne `"12.3 m"` par exemple
        - `input().split()` renvoie une liste du découpage aux espace,
            - ici c'est `["12.3", "m"]` qui est renvoyée.
            - `nombre_txt, unite = input().split()` réalise deux affectations
                - `nombre_txt = "12.3", puis `unite = "m"`
            - `nombre = int(nombre_txt)` permet d'avoir `nombre = 12.3` et de faire de calculs.

    ```output
    40.354331 p
    2.745761 l
    98.960000 f
    0.050715 l
    ```

!!! tip "Conseils"
    1. Ici, la lecture de l'entrée est très délicate pour un débutant. Il vaut mieux avoir commencé les sections `Tableau` et `String`
    2. Il faut faire attention à vos fonctions de conversion. Ne vous trompez pas de sens. Testez-les !

