# Sommaire

!!! warning "Difficultés"
    Cette section aborde plus d'aspects mathématiques.

    Il est aussi possible de passer à la section suivante qui est indépendante.


!!! abstract "Menu"
    - Quelques fonctions disponibles avec Python
    - Le module `math`
    - Comment créer des fonctions
    - Des exercices mathématiques

{{ IDE() }}
