# ❗ La fonction factorielle

## Définitions et exemples

La factorielle d'un entier $n$ se note $n!$ et est égale au produit des entiers de $1$ jusqu'à $n$.

- $5! = 1×2×3×4×5 = 120$
- $4! = 1×2×3×4 = 24$
- $3! = 1×2×3= 6$
- $2! = 1×2= 2$
- $1! = 1$
- $0! = 1$, comme un produit vide.

On remarque que $5! = 4! × 5$, et de manière générale :

$$n! = \begin{cases}1 &\text{si } n = 0\\(n-1)! × n &\text{si } n\geqslant 1\end{cases}$$

## Exercice

1. Compléter la définition de `factorielle`,
    - d'après la propriété,
    - il suffit de remplacer ...
    
2. Exécuter le script et vérifier

    ```pycon
    >>> factorielle(3)
    6
    >>> assert factorielle(5) == 120
    >>> f"4! = {factorielle(4)} ; l'image de 4"
    '4! = 24 ; l'image de 4'
    ```

3. Lancer la fonction `message`
   et essayer de la comprendre.

    ```pycon
    >>> message()
    ```

{{ IDE('factorielle') }}

??? done "Solution"
    Dans le script : `#!python return factorielle(n - 1) * n`

    Quelques pistes pour `message()`.

    - `message()` ne prend aucun paramètre, il faut toutefois des parenthèses vides pour utiliser la fonction.
    - `print` est une fonction Python pour **afficher**.
    - `#!python for ... in ... :` est la construction pour répéter en boucle des instructions.
    - Le décalage (l'**indentation**) est indispensable pour indiquer le début et la fin d'un bloc d'instructions, pour les fonctions, comme pour les structures conditionnelles, comme pour les boucles.
    - Il y a d'autres constructions que nous aborderons ensuite... Patience.

!!! danger "Défi :boom:"
    On rappelle que $100! = 1×2×3×4×5× \dots ×98×99×100$

    En s'inspirant d'une remarque évoquée dans la section `comparaison`

    - justifier que $10^{30} \leqslant 100! < 10^{300}$, puis
    - par dichotomie, trouver le meilleur encadrement par des puissances de $10$,
    - en déduire le nombre de chiffres de $100!$.

!!! info "Fonction récursive"
    Les fonctions récursives ne sont pas à étudier au programme en Première,
    cependant elles sont parfois plus simples que les versions itératives.

    On peut donc les présenter dans les cas les plus simples, elles seront étudiées un peu plus en Terminale.
