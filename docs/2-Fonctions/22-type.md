# 🦎 `type` et fonctions de conversion

Il est possible de changer explicitement le type d'une variable.

```pycon
>>> int(5.0)    #    5.0 est un (1)
5
>>> int(5.8)   #   5.8 est un (2)
5
>>> float(8)  # 8 est un (3)
8.0
```

1. flottant, on le convertit en entier, ici de même valeur
2. flottant, on le convertit en entier, ici **tronqué**.
3. entier, on le convertit en flottant de même valeur.

!!! warning "Conversion implicite"
    Python peut aussi le faire implicitement.

    ```python
    def est_nul(n):
        "Renvoie un booléen : le nombre n est-il nul ?"
        if n:
            return False
        else:
            return True
    ```

    `#!python if` attendait un booléen, Python peut convertir implicitement
    
    - un nombre non nul en `True`,
    - et un nombre nul en `False`.

    La conversion implicite est parfois très pratique comme dans `#!python 5 + 0.2` : l'addition d'un entier et d'un flottant. L'entier est d'abord converti **implicitement** en flottant, **puis** l'addition a lieu.

!!! done "Bonne pratique"
    Dans certains langages stricts les conversions de type doivent toutes être **explicites**.

    La conversion explicite est une bonne pratique ! Cela évite certains bogues, ils sont même détectés **avant** le lancement du programme.

## Les types numériques

`bool, int, float, complex` sont des types numériques et aussi des fonctions de conversion pour passer quand c'est possible d'un type à un autre (booléen, entier, flottant ou complexe).

=== "Exemple"

    ```pycon
    >>> bool(8.5)  # (1)
    True
    >>>
    ```

    1. 8.5 est non nul, donc convertit en `True`

=== "Contre-exemple"

    ```pycon
    >>> float(10**10000)  # (1)
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    OverflowError: int too large to convert to float
    >>>
    ```

    1. Le plus grand entier que l'on peut convertir en flottant dépend de la version compilée de Python

    > Erreur de débordement : l'entier est trop grand pour être converti en flottant.

    Pour en savoir plus sur les types numériques : [programiz (_in english_)](https://www.programiz.com/python-programming/numbers){ target=_blank }


## Les types de données structurées

`tuple, str, list, set, dict` sont des types de données, et aussi des fonctions de conversion.

- `tuple` pour un couple, un triplet, ou plus généralement un [n-uplet](https://fr.wikipedia.org/wiki/N-uplet).
- `str` pour une chaine de caractère.
- `list` pour un tableau ou une liste ordonnée.
- `set` pour un ensemble ; non ordonné, sans doublon.
- `dict` pour un dictionnaire ; un ensemble de clés et la valeur associée à chaque clé.

:warning: Dans les Parties 1 et 2, on se limite volontairement. On n'étudie qu'un pan des possibilités des `list`, des `tuple`, des `str` et des `dict`. On n'aborde pas du tout `set`, sauf pour en faire des allusions.

## Remarques techniques

!!! danger "Section délicate"
    Cette section est réservée à ceux qui veulent en savoir davantage.

### Obtenir le type d'un objet

!!! example "Exemples expliqués"
    ```python
    In [1]: type(1)
    Out[1]: int

    In [2]: type(1.)
    Out[2]: float

    In [3]: 1. is 1
    Out[3]: False

    In [4]: 1. == 1
    Out[4]: True
    ```

    !!! info "Remarques"
        1. `1` est de type entier, (_**int**eger_)
        2. `1.` ou bien `1.0` est de type flottant, (_**float**ing point number_)
        3. Ce **ne sont pas** les mêmes objets en interne pour Python. `#!python is` répond alors `False` pour faux.
        4. À la comparaison, il se passe un phénomène de changement de type. Pour être comparé à un flottant, un entier est automatiquement converti en flottant. Et là, la comparaison s'avère égale, donc le test d'égalité renvoie `#!python True` (pour vrai). Nous avons aussi évoqué ce phénomène pour une opération entre un flottant et un entier.

!!! danger "Cas des flottants"
    ```python
    In [22]: 0.1 + 0.0045
    Out[22]: 0.1045

    In [23]: (5.4 + 2.7)
    Out[23]: 8.100000000000001

    In [24]: (5,4 + 2,7)
    Out[24]: (5, 6, 7)
    ```

    - Le premier exemple montre ce qu'il se passe fréquemment : l'affichage décimal de la somme de deux flottants (issus de décimaux) est égale à la somme des décimaux d'origine. *Cette phrase était complexe ; reformulons*. Dit autrement : l'addition de deux décimaux, provoque en machine l'addition de deux nombres en binaires qui ne sont pas égaux aux décimaux, mais la somme calculée (qui sera un nombre en binaire) peut s'écrire souvent en décimal comme la somme des décimaux d'origine.
    - Dans le second exemple, on constate que ce n'est pas une généralité.
    - Dans le troisième exemple, Python a affiché les trois éléments d'un *tuple*, le second étant 4+2, égal à 6. Nous verrons les tuples juste après. Ici, il n'y a pas d'erreur à l'exécution, on parlera éventuellement d'erreur sémantique.
