def sommation(ma_liste: list) -> int:
    cumul = 0
    for nombre in ma_liste:
        cumul =  cumul + nombre
    return cumul
