CONST_voyelles = "aeiouyAEIOUY"

def est_voyelle(lettre):
    return lettre in CONST_voyelles

def conjugue(verbe):
    """Écris avec print la conjugaison de `verbe`,
    après avoir vérifier que c'est un verbe du 1er groupe.
    La conjugaison est donnée au présent de l'indicatif,
    pour un verbe classé comme `aimer`.
    """
    taille = len(verbe)
    assert taille > 2, f"{verbe} n'est pas un verbe !"
    racine = verbe[0 : taille - 2]
    suffixe = verbe[taille - 2 : taille]
    assert suffixe == "er", f"{verbe} ne se termine pas par 'er' !"

    # On suppose que `verbe` est un verbe correct du premier groupe comme "aimer"
    première_lettre = racine[0]
    if est_voyelle(première_lettre):
        print("J'" + racine + "e.")
    else:
        print("Je " + racine + "e.")
    print("Tu " + racine + "es.")
    print("Il/Elle/On " + racine + "e.")
    print("Nous " + racine + "ons.")
    print("Vous " + racine + "ez.")
    print("Ils/Elles " + racine + "ent.")


# Exemple
conjugue('coder')
