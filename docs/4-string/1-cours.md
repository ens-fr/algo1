# 📑 Chaine de caractères

!!! info "Pas de type `char` en Python"
    En Python, il n'y a pas de type à part pour les caractères **seuls**. Ce sont des chaines de caractères de taille 1.

En Python, les chaines de caractères sont du type `#!python str`, pour _string_. Tous les caractères Unicode [^uni] sont acceptés.

[^uni]: :fontawesome-brands-wikipedia-w: [Le standard Unicode](https://fr.wikipedia.org/wiki/Unicode){ target=_blank } permet des échanges de textes dans différentes langues, à un niveau mondial.

```pycon
>>> msg_bienvenue = "Bonjour à tous !"
>>> type(msg_bienvenue)
<class 'str'>
>>>
```

## Création

### En dur

On peut créer en dur (_hard code_) une chaine de caractères de plusieurs façons.

- En encadrant le texte entre deux `"`. La bonne idée, simple. Mais sans `"` au milieu.
- En encadrant le texte entre deux `'`. :warning: le texte ne doit pas contenir de `'` que **l'on** utilise souvent en français.
- En encadrant le texte de deux `"""`. La bonne idée pour un texte plus complexe.
    - Plusieurs lignes possibles.
    - Présence possible de guillemets simples `'` ou doubles `"`.

```pycon
>>> phrase_simple = "Méthode recommandée en français"
>>> variante = 'Méthode possible, mais non recommandée en français'
>>> phrase_longue = """Une "façon" d'écrire
... sur "plusieurs" lignes,
... avec éventuellement des guillemets.
... """
>>>
```

!!! cite "Guillemets"
    - simple `'` , ce n'est pas une apostrophe dactylographique, mais on s'en sert comme ça au clavier...
    - double `"`, à ne pas confondre avec `''` deux guillemets simples...


### Par conversion

On peut créer une chaine de caractères en utilisant la fonction native `#!python str` sur de nombreux objets Python.

```pycon
>>> point_M = (512, 824)
>>> str(point_M)
'(512, 824)'
>>> x, y = point_M
>>> x
512
>>> str(x)
'512'
```

### Par concaténation

On peut concaténer deux chaines de caractères avec l'opérateur `+`.

```pycon
>>> "L'abscisse du point M est : " + str(x) + " et son ordonnée est : " + str(y) + "."
"L'abscisse du point M est : 5 et son ordonnée est : 8."
```

On peut aussi répéter une concaténation avec l'opérateur `*`.

```pycon
>>> 3 * 'bla'
'blablabla'
```

### Avec des _f-string_

On **préfèrera** souvent utiliser les _f-string_ dans ces situations.

```pycon
>>> f"L'abscisse du point M est : {x} et son ordonnée est : {y}."
"L'abscisse du point M est : 5 et son ordonnée est : 8."
```

### Entrée par l'utilisateur

On peut aussi récupérer du texte de l'utilisateur avec `input`

```pycon
>>> prénom = input("Quel est ton prénom ? ")
Quel est ton prénom ? Franck
>>>
```

!!! info "Comment récupérer un nombre ?"
    Pour récupérer un nombre entré, on le récupère **en texte**,
     puis on le convertit en nombre.

    ```pycon
    >>> nombre_txt = input("Entre un entier : ")
    Entre un entier : 42
    >>> nombre = int(nombre_txt)
    >>>
    ```

    Dans certains cas il est possible de combiner les deux actions :

    ```pycon
    >>> nombre_entier = int(input("Entre un entier : "))
    >>> nombre_flottant = float(input("Entre un décimal : "))
    >>>
    ```

    On rappelle (et on le reverra) qu'_a priori_ le nombre flottant stocké sera légèrement différent du nombre décimal entré.

    - En mathématiques, on ferra confiance à l'utilisateur et on ne gèrera pas le cas où il tente d'entrer du texte à la place des chiffres.
    - En NSI, on étudiera comment obtenir une entrée qui vérifie certains critères.

## Opérations simples

### Itération directe

On peut utiliser une chaine de caractères **dans** une boucle `#!python for`

```python
mot = "former"
for lettre in mot:
    print("Une lettre :", lettre, "et son code ASCII", ord(lettre))
```

    Une lettre : f et son code ASCII 102
    Une lettre : o et son code ASCII 111
    Une lettre : r et son code ASCII 114
    Une lettre : m et son code ASCII 109
    Une lettre : e et son code ASCII 101
    Une lettre : r et son code ASCII 114


### Longueur d'une chaine de caractères

La fonction native `#!python len` renvoie la taille de différents objets Python, comme ceux de type `#!python str`, `#!python list`, `#!python set`, `#!python dict`.

```pycon
>>> mot = "former"
>>> len(mot)
6
```

### Accès à un caractère d'indice donné

Avec une chaine `phrase` de taille `#!python len(phrase)`, les caractères sont accessibles avec les indices de `0` **inclus**, jusqu'à `#!python len(phrase)` **exclu**.

On accède à un caractère de `phrase` d'indice `i` avec `#!python phrase[i]`

```pycon
>>> mot = "former"
>>> len(mot)
6
>>> mot[0]
'f'
>>> mot[5]
'r'
>>> mot[6]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: string index out of range
>>>
```

Le dernier exemple indique :

> Erreur d'indice : l'indice de la chaine est en dehors de l'intervalle.

En effet, on accède à `mot` par indice de $0$ **inclus** à $6$ **exclu**.

### Modifier une chaine de caractères

:warning: **Ce n'est pas possible.**

On pourra cependant créer une autre chaine à partir d'une autre et utiliser la même variable.

```pycon
>>> mot = "form"
>>> mot = mot + "er"
>> mot
'former'
```

:warning: On dirait que `mot` a été modifiée ! C'est faux !

1. La chaine `"form"` a été créée, la variable `mot` pointe dessus.
2. `mot + "er"` crée une **nouvelle** chaine `'former'`, puis la variable `mot` pointe sur ce résultat.

!!! danger "Pourquoi cette nuance ?"
    L'objet chaine de caractère n'est pas modifiable, on le dit **immuable**.

    Une fois créée, une chaine possède une signature, un _hash_.

    Ce _hash_ est utilisé pour distinguer **très rapidement** une chaine d'une autre.

    Le principe de figer l'objet est **indispensable** pour créer des ensembles ou des clés de dictionnaire.

    De même les objets de type numérique sont **immuables**, même si une variable peut pointer successivement vers différentes valeurs. Le nombre $42$, par exemple, restera toujours le nombre $42$.

    Le type `list`, lui, donne des objets **mutables**.

    !!! faq "Clé de dictionnaire"
        - Une ==chaine de caractères pourra== être une clé de dictionnaire.
        - Une ==liste **ne** pourra **pas**== être une clé de dictionnaire.
        

### Itération avec indice

On peut donc aussi utiliser l'indice de chaque caractère pour une boucle.

```python
mot = "former"
taille = len(mot)
for i in range(taille):
    print("Le caractère d'indice", i, "est", mot[i])
```

    Le caractère d'indice 0 est f
    Le caractère d'indice 1 est o
    Le caractère d'indice 2 est r
    Le caractère d'indice 3 est m
    Le caractère d'indice 4 est e
    Le caractère d'indice 5 est r

