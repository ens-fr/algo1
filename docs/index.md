# 🏡 Accueil

L'essentiel à savoir pour découvrir l'algorithmique avec un peu de mathématiques.

- Pour tous les élèves en spé maths ou en spé NSI première.

![logo Python](./images/logo-python.svg){width=300}

Le cours utilise Python comme langage de programmation pour les exemples.

{{ terminal() }}

> Un grand remerciement à M. Vincent BOUILLOT pour le portage de [Pyodide](https://pyodide.org/en/stable/) vers MkDocs, cela permet d'utiliser Python encore plus simplement qu'avec [Basthon](https://basthon.fr/){ target=_blank }.

![Exemples avec divisions](./images/divisions.svg)

> Un exemple d'utilisation ; des calculs simples.

!!! done "Première partie"
    - On progresse avec les niveaux 1 et 2, et la moitié du niveau 3 sur [France-IOI](http://www.france-ioi.org/algo/chapters.php).
    - On propose des activités ludiques.
    - On propose des activités mathématiques.

!!! example "Les sections"
    - La section `Découverte` reprend la progression du niveau 1 de France-IOI.
        - Elle est adaptée à tous les lycéens, voire des collégiens.
    - La section `Calculatrice` aborde Python avec un angle plus mathématique.
        - Elle est adaptée aux élèves de première spé Maths ou NSI.
    - La section `programme` aborde une partie du niveau 2 et se concentre sur les parties plus mathématiques, avec plus de travail sur les fonctions.
    - La section `Tableau` sera utile aux élèves de spé maths, mais aussi spé NSI.
    - Les autres sections `String` et `Dictionnaire` sont destinées aux élèves de NSI.
        - On termine le niveau 2 et on entame le niveau 3 de France-IOI.

!!! done "Deuxième partie"
    Dans la [deuxième partie](https://ens-fr.gitlab.io/algo2/){ target=_blank }, on finit le niveau 3, et on aborde les niveaux 4 et 5 de France-IOI.



---

!!! info "À propos de ce site - RGPD"
    - Aucun _cookie_ n'est créé.
    - Il n'y a aucun lien vers des pisteurs.
        - Pas de polices Google qui espionne.
        - Pas de CDN malicieux.
    - Le langage Python est émulé par Pyodide sur votre propre machine.
        - Vous n'avez besoin de rien installer.
        - Strictement aucun code, aucune donnée personnelle n'est envoyée.
    - Adapté pour les PC, les tablettes et aussi les téléphones. 
    - Très peu gourmand en ressources, les pages sont légères.
    - Code source du contenu sous licence libre, sans utilisation commerciale possible.
