# 🧾 Liste Python

!!! info "Tableau ou liste ?"
    On a déjà vu l'utilisation bridée des listes Python, comme des tableaux.

    Un tableau :

    - possède une taille fixée à sa création,
    - possède des éléments de même type,
    - a ses éléments indicés et accessibles en lecture ou écriture,
    - les indices vont de $0$ inclus, jusqu'à la taille **exclue**.

    Avec les listes Python, on peut ajouter ou supprimer des éléments. La taille n'est donc pas fixe.

    - On peut à nouveau décider de se restreindre, et choisir d'ajouter ou supprimer **uniquement à la fin**, un élément à la fois. C'est ce que nous choisirons de faire.


Ces méthodes utilisent la notation de la programmation orientée objet, avec un `.` qui indique à gauche le parent et à droite sa filiation.

## Ajout d'un élément

À **une liste**, on peut **ajouter** un **élément**.

```pycon
>>> une_liste = [2, 3, 5, 7]
>>> une_liste.append(11)
>>> une_liste
[2, 3, 5, 7, 11]
```

`ma_liste.append(n)` ajoute `n` à `ma_liste`

!!! info "Notation POO"
    Avant la programmation orientée objet, on utilisait une écriture du genre `ajoute(élément, ma_liste)` ou alors `ajoute(ma_liste, élément)` ; aucune des deux n'est satisfaisante dès que les choses se compliquent.

### Exemple d'utilisation

Voici un programme qui : 
1. Crée une liste vide `mes_carrés`,
2. lit un entier `nb_lignes`,
3. lit `nb_lignes` lignes contenant un entier `nombre`,
4. pour chaque `nombre` lu, ajoute son carré à la liste uniquement s'il est pair.

```python
nb_lignes = int(input())
mes_carres = list() # variante de ma_liste = [] pour une liste vide
for _ in range(nb_lignes):
    nombre = int(input())
    carre = nombre * nombre
    if carre % 2 == 0:
        mes_carres.append(carre)
```

On rappelle qu'on préfère les listes en compréhension avec le code suivant : 

```python
nb_lignes = int(input())
ma_liste_lue = [int(input()) for _ in range(nb_lignes)]
mes_carrés = [nombre * nombre for nombre in ma_liste_lue if nombre * nombre % 2 == 0]
```

Cependant, on constate qu'avec les listes en compréhension, on fait deux fois le calcul `nombre * nombre` pour chaque cas validé ; cela pourrait être gênant et lent, d'autre part le code n'est pas factorisé...

> La méthode `.append()` est parfois très utile.

## Suppression d'un élément

* `ma_liste.pop()` renvoie en supprimant le **dernier** élément de `ma_liste`

```pycon
>>> ma_liste = [2, 3, 5, 7]
>>> ma_liste.pop()
7
>>> ma_liste.pop()
5
>>> ma_liste
[2, 3]
```

