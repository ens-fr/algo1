# Sommaire

On propose un cours moderne, qui peut être lu facilement sur tablette, téléphone ou ordinateur.

Toute cette section **Découverte** reprend la progression sur France-IOI, niveau 1.

!!! tip "France-IOI"
    Si ce n'est pas déjà fait, s'inscrire sur le site [France-IOI](http://www.france-ioi.org/index.php), cela vous permet de conserver la trace de votre progression. Il est vivement conseillé de s'y exercer régulièrement.

    > [France-IOI est une association](http://www.france-ioi.org/asso/index.php) à but non lucratif (loi 1901) fondée en juin 2004 dans le but de développer la sélection et l'entraînement de l'équipe de France aux Olympiades Internationales d'Informatique.

    Le site est aussi adapté à la découverte de la programmation, avec le langage Python. Le niveau 1 est accessible à tous les élèves du lycée. Les niveaux 2 et 3 intéressent les élèves en spé maths ou NSI. Les niveaux 3, 4 et 5 proposent un joli contenu pour les élèves de terminale NSI.

!!! done "Bientôt un jeu"
    Quand vous aurez presque **fini** cette section `Découverte`, il vous sera possible de faire jouer un personnage à un jeu grâce à Python.

    ![pyRATES](images/pyrates.png)
