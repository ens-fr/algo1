# 🏴‍☠️ pyRATES

!!! done "BRAVO"
    Avec Python, vous savez désormais :

    - Utiliser des variables et enchainer des calculs simples
    - Lire et écrire en console des données
    - Répéter une action un certain nombre de fois
    - Utiliser une condition pour choisir ses actions
    - Répéter une action tant qu'une condition est réalisée


Vous pouvez désormais tenter les chalenges de pyRATES, en cliquant sur l'image.

[![pyRATES](images/pyrates.png)](https://py-rates.fr/){ target=_blank }

Il vous faudra raisonnablement entre 20 minutes et un peu plus d'une heure.

## La suite

!!! info "Créateur de jeux vidéos"
    Jouer, c'est bien, mais créer c'est encore mieux !
    
    Mais soyons honnêtes, il ne sera pas possible très vite de créer des jeux en 3D avec beaucoup d'actions...

    En revanche, il sera possible de créer des petits jeux simples, en mode texte, à un ou deux joueurs. Plus le jeu sera complexe, et plus il faudra utiliser de mathématiques pour le construire efficacement.


Il faudra poursuivre avec la section `Calculatrice`, au moins jusqu'à `Comparaison`. Ensuite, le contenu est plutôt pour les élèves en spécialité mathématiques.

La section `Programme` vous apprendra à utiliser de nouvelles fonctions et d'en **créer**. Seule la fin de cette section contient de vrais exercices de mathématiques.

Dès à présent, il est possible de créer des jeux simples.
