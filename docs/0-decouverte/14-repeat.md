# 🔁 Répétition d'instructions

Le premier gros avantage de l'informatique, c'est de pouvoir **automatiser** des tâches répétitives.

## Répéter n fois

Avec Python, pour répéter $n$ fois une même action, on peut écrire

```python
for tour in range(n):
    action()
```

## Première utilité

Pour déplacer un robot de 10 cases à droite, deux méthodes.

=== "Avec boucle"

    ```python
    from robot import *

    for _ in range(10):
        droite()
    ```

=== "Sans boucle"

    ```python
    from robot import *

    action()
    action()
    action()
    action()
    action()
    action()
    action()
    action()
    action()
    action()
    ```

Faire une boucle est plus rapide à écrire, à gérer... Imaginez avec 1000 répétitions...


```python
for tour in range(nb_tours):
    ######################
    #                    #
    des_actions()        #
    #                    #
    ######################
suite_du_programme()
```



!!! abstract "La syntaxe"
    * On commence par `#!python for`, un mot clé du langage Python.
    * Ensuite, on choisit un **nom de variable**.
        - C'est bien de choisir un nom qui a du sens, mais pas un mot-clé.
        - On recommande `tour` pour commencer.
    * Ensuite, il y a `#!python in` ; un mot-clé du langage.
    * Puis `#!python range(...)` dans lequel on écrit le nombre de tours à faire.
    * **Très important**, on termine la ligne par `:`

    Les lignes suivantes, du bloc d'instructions que l'on veut répéter doivent être **décalées**.

    On revient au **même niveau** que le `#!python for` initial, pour la **suite** des instructions.

## Exemples

Essayez chacun des exemples, vous pouvez les modifier.

=== "Exemple 1"

    {{ IDEv('for1') }}

=== "Exemple 2"

    {{ IDEv('for2') }}

=== "Exemple 3"

    {{ IDEv('for3') }}

## Exercice 1

{{ IDEv() }}

Votre objectif est d'afficher le texte ci-dessous en faisant des boucles.

=== "Situation 1"

    ```output
    ***************************************************
    ```

    Une boucle suffit.

=== "Situation 2"

    ```output
    ***************************************************
    *                        O                        *
    ***************************************************
    ```

    4 boucles pour cette situation.

La largeur est de 51 (mais imaginez qu'on vous demande 5001 bientôt).


## Exercice 2

Combien de fois l'instruction `action` est-elle effectuée dans chaque cas ?

=== "Cas n°1"

    ```python
    for tour in range(10):
        action()
    ```

    ??? done "Réponse"
        $10$ ; facile.
    
=== "Cas n°2"

    ```python
    init()
    action()
    for tour in range(12):
        action()
        inter()
        action()
    fin()
    action()
    ```

    ??? done "Réponse"
        - $1$ fois avant
        - $12$ fois $2$ pendant
        - $1$ fois après

        Il y a eu $26$ fois l'action `action` effectuée.

=== "Cas n°3"

    ```python
    for tour_1 in range(10):
        action()
        for tour_2 in range(5):
            action()
            post()
    action()
    ```

    Oui, on peut faire une boucle dans une boucle. À méditer !

    ??? done "Réponse"
        * $10$ fois ($1$ plus $5$)
        * plus une dernière

        Un total de $61$ fois l'action `action` effectuée.

=== "Cas n°4"

    ```python
    for tour_1 in range(1000):
        action()

    for tour_2 in range(1000):
        action()

    for tour_3 in range(1000):
        action()
    ```

    ??? done "Réponse"
        $1000+1000+1000$ donne $3000$ fois l'action `action` effectuée.
        
=== "Cas n°5"

    ```python
    for tour_1 in range(1000):
        for tour_2 in range(1000):
            for tour_3 in range(1000):
                action()
    ```

    ??? done "Réponse"
        Ici, c'est $1000×1000×1000$ donc un milliard de fois où l'action est effectuée. Ce qui montre le danger d'écrire ce genre de construction. À méditer !

=== "Cas n°6"

    ```python
    for tour_1 in range(1000):
        action()
        for tour_2 in range(1000):
            action()
            for tour_3 in range(1000):
                action()
    ```

    ??? done "Réponse"
        Ici, c'est $1000×(1 + 1000×(1 + 1000))$ donc $1\,001\,001\,000$
