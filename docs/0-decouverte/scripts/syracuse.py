def terme_suivant(u_n):
    """""Renvoie le terme suivant `u_n`
       dans une suite de Syracuse
    """
    
    if u_n % 2 == 0:
        return u_n // 2
    else:
        return 3 * u_n + 1
