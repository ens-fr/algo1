# 🗺️ Hello World!

## Une tradition

La tradition pour un premier programme informatique est d'afficher `Hello World!`

Avec Python, le langage que nous utiliserons.

{{ IDEv('hello') }}

- Pour lancer le script, appuyer sur : <button class="emoji">⚙️</button> 

!!! info "Et avec d'autres langages ?"
    Juste pour information.

    === "C"

        ```c
        #include <stdio.h>

        int main() {
        printf("Hello World!");
        return 0;
        }
        ```

        C est le langage incontournable du développement système.

    === "C++"

        ```cpp
        #include <iostream>

        int main() {
            std::cout << "Hello World!";
            return 0;
        }
        ```

        C++ est une évolution du C pour faire de la programmation orientée objet efficacement.

    === "C#"

        ```c#
        namespace HelloWorld
        {
            class Hello {
                static void Main(string[] args)
                {
                    System.Console.WriteLine("Hello World!");
                }
            }
        }
        ```

        Basé sur C++ et Java, fabriqué par MicroSoft.

    === "Pascal"

        ```pascal
        program Hello;
        begin
        writeln ('Hello, world!');
        end.
        ```

        Ancien langage de programmation.



    === "Haskell"

        ```haskell
        module Main where

        main :: IO ()
        main = putStrLn "Hello World!"
        ```

        Langage fonctionnel, basé sur les mathématiques.

    === "Assembleur"

        ```amdgpu
            global _main
            extern _printf

            section .text
        _main:
            push    message
            call    _printf
            add        esp, 4
        message:
            db    'Hello World', 10, 0
        ```

        Langage proche du langage machine.

    === "Java"

        ```java
        /*package whatever //do not write package name here */

        import java.io.*;

        class GFG {
            public static void main (String[] args) {
            System.out.println("Hello World!");
            }
        }
        ```

        Langage célèbre.

    === "JavaScript"

        ```js
        console.log("Hello World!");
        ```

        Langage très utilisé en complément du HTML pour rendre dynamique le Web.

    === "Fortran"

        ```fortran
        program helloworld
            print *, "Hello World!"
        end program helloworld
        ```

        Ancien langage.

    === "BASH"

        ```bash
        echo "Hello World!"
        ```

        Langage du terminal de commande.

    === "HTML"

        ```html
        <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
        <h1>Hello World!<h1>
        </body>
        </html>
        ```

        Langage pour les pages Web.

    === "Markdown"

        ```markdown
        Hello World!
        ```

        Simplification du HTML. Oui, c'est tout :smile:


## L'affichage avec Python

On peut afficher

-   du texte entre guillemets
-   des nombres
-   d'autres objets...

On peut afficher plusieurs objets à la suite avec un seul `print`.

=== "Exemple 1"

    ```python
    print(123, 456,     789,   000000)
    print(     "000"         )
    ```

    ```output
    123 456 789 0
    000
    ```
    
    -   À la fin de la première ligne, il n'y a qu'un `0` affiché, c'est le **nombre** $0$.
    -   La seconde ligne affiche le **texte** `000`, c'était entre guillemets, Python ne l'a pas converti en nombre avant affichage. Ce texte est bien calé à gauche.

=== "Exemple 2"

    ```python
    print("Si x =",5,"et y =", 8, ", alors x + y =", x + y)
    print("Simple",          "éloigné")
    ```

    ```output
    Si x = 5 et y = 8, alors x + y = 13
    Simple éloigné
    ```
    
    - Regarder les espaces dans le code, et dans le résultat.
    - Ça ne change rien, Python met **une** seule espace entre les objets affichés.
    - La bonne pratique est de mettre une espace après la virgule dans le code.
    - On apprendra à modifier le séparateur pour un affichage différent.

Il y a trois choses à signaler :

1.  Peu importe la séparation que vous utilisez entre les paramètres.
2.  `print` sépare les objets avec **une** seule espace.
3.  `print` termine par un saut de ligne.

!!! danger "On peut changer ce comportement"

    ```python
    print("mot", 12345, "colle", 7, sep="", end="")
    print("fin")
    ```

    ```output
    mot12345colle7fin
    ```

    1.  Le premier `print` a affiché chaque objet en les séparant (`sep`) avec du vide `""` (au lieu de l'espace), et a fini (`end`) avec du vide (au lieu du saut de ligne).
    2.  Le second `print` enchaine donc avec `fin` collé. Mais il termine avec un saut de ligne, lui !

### Exercice 1

Anticiper l'affichage produit par ce programme

```python
print("abc", 123, "567", 8, sep="...", end="===")
print(42, 1337, sep=" T ")
```

??? done "Affichage"

    ```output
    abc...123...567...8===42 T 1337
    ```

### Exercice 2

{{ IDEv('affiche') }}

??? done "Réponse"

    ```python
    print(0, 1, 1, 2, 3, 5, 8, 13, 21, sep=" | ")
    ```

    On a juste ajouté `, sep=" | "`

!!! info "Utilisation dans le développement web"
    Cette technique est utilisée pour créer des pages web dynamiquement.
    On écrit un programme qui produit automatiquement du code HTML en fonction de données dans un tableau. On peut aussi créer du Markdown ou tout autre sortie qui est envoyée à un autre programme.

    Bienvenue dans le monde de la programmation ! :smile:

## L'essentiel

!!! savoir "À retenir"
    1.  Pour afficher un collage de morceaux de texte, on peut faire :
      
        ```python
        print("début", end="")
        print("suite1", end="")
        print("suite2", end="")
        print("fin")
        ```

        ```output
        débutsuite1suite2fin
        ```
        Cette technique permet de construire une ligne en plusieurs étapes.
    2.  Pour afficher une ligne vide, il suffit de faire
        
        ```python
        print("")
        ```

        ou alors

        ```python
        print()
        ```
