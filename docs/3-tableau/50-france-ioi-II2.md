# 👾 France-IOI (II-2)


Le niveau 2 de France-IOI se poursuit avec un travail sur les tableaux.
[Découverte des tableaux](http://www.france-ioi.org/algo/chapter.php?idChapter=651)

Voici un peu d'aide pour commencer.

## 1) Préparation de l'onguent

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=2068) est :

>  Il y a 10 ingrédients dans la recette et les quantités nécessaires pour chacun sont (en grammes) : 500, 180, 650, 25, 666, 42, 421, 1, 370 et 211.
>
> Votre programme doit lire un entier, le numéro d'un ingrédient (compris entre 0 et 9) et afficher la quantité associée à cet ingrédient.

??? tip "Indice"
    Modifier et compléter le code

    ```python
    dosage =   TABLEAU  500, 180, 650, 25, 666, 42, 421, 1, 370 et 211
    id_ingredient = int(input())
    dose = ... dosage ... id_ingredient ...
    print(dose)
    ```

## 2) Liste de courses

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=5) est :

>  Il y a 10 ingrédients et ils ont tous un prix au kilo différent : 9, 5, 12, 15, 7, 42, 13, 10, 1 et 20.
>
> Votre programme devra lire 10 entiers, le poids (en kilogrammes) qu'il faut acheter pour chaque ingrédient. Il devra calculer le cout total de ces achats. 


??? tip "Indice"
    Modifier et compléter le code

    ```python
    PRIX_KILO = ... 9, 5, 12, 15, 7, 42, 13, 10, 1 et 20...
    ...
    RÉPÉTER 10 FOIS
        poids_ingredient = int(input())
        prix_ingredient = ...
        ...
    print(prix_total)
    ```


## 3) Grand inventaire

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=7) est :

> Déterminer pour chaque produit, la quantité restant dans le stock à l'issue de l'ensemble de ces achats et ventes. 

??? tip "Indice"

    ```python
    stock =   TABLEAU AVEC 10 ou 11 ZÉROS
    nb_operations = int(input())
    RÉPÉTER nb_operations FOIS
        id_ingredient = int(input())
        variation = int(input())
        ...
    
    POUR i DANS [1..10] ou bien [0..9]
        print(stock DE INGRÉDIENT i)
    ```

## 4) Étude de marché

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=9) est :

> On vous donne le numéro du produit préféré par différentes personnes. Écrivez un programme qui indique pour chaque numéro de produit, le nombre de personnes dont c'est le produit préféré. 


??? tip "Indice"
    Modifier et compléter le code

    ```python
    nb_produits = int(input())
    nb_personnes = int(input())

    ??? = TABLEAU des effectifs des préférences pour chaque produit

    RÉPÉTER nb_personnes FOIS
        choix = int(input())
        ...
    
    RÉPÉTER nb_produits FOIS avec variable i
        print(??? du produit i)
    ```


## 5) Répartition du poids

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=10) est :

> Déterminer quel poids ajouter ou retirer à chaque charrette, pour qu'elles transportent toutes ensuite le même poids, et ce, sans modifier le poids total transporté par l'ensemble des charrettes de la caravane.


??? tip "Indice"
    Modifier et compléter le code

    ```python
    nb_charrettes = int(input())
    ??? TABLEAU des poids des charrettes

    ...
    RÉPÉTER nb_charrettes FOIS
        poids = int(input())
        ...
    ...

    RÉPÉTER nb_charrettes FOIS
        variation = ???
        print(variation)
    ```


## 6) Visite de la mine

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=11) est :

> D'afficher la suite des déplacements à faire pour aller de votre position actuelle à la sortie. 


??? tip "Indice 1"
    Modifier et compléter le code

    ```python
    nb_deplacements = int(input())
    ??? TABLEAU des déplacements

    RÉPÉTER nb_deplacements FOIS
        deplacement = int(input())
        ...

    ...
    RÉPÉTER nb_deplacements FOIS
        deplacement = ???
        ...
    ```
    
??? tip "Indice 2"
    Pour l'affichage, il y a deux possibilités.

    1. On peut faire une structure conditionnelle qui traite les 6 cas.
    2. On peut utiliser un **autre tableau** qui inverse les déplacements.


## 7) Journée des cadeaux

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=14) est de :

Calculer la médiane d'une liste.

??? tip "Indice"
    Modifier le code

    ```python
    effectif = int(input())
    fortune = TABLEAU de taille effectif
    ...
    SI effectif est impair
        i_mediane = ...
        mediane = fortune DE i_mediane
    SINON
        i_mediane_1 = ...
        i_mediane_2 = ...
        mediane = ...
    
    print(mediane)
    ```

    Remarquez qu'on utilise un seul `print` dans le code.

    On ne mélange pas la variable `i_mediane` et la variable `mediane`.


## 8) Course à trois jambes

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=16) est de :

> D'afficher la composition de chacune des équipes, dans l'ordre.

??? tip "Indice"
    Modifier le code

    ```python
    nb_participants = int(input())
    choix = TABLEAU de taille nb_participants
    RÉPÉTER nb_participants FOIS
        numero = int(input())
        choix ??? = ...
    
    ...

    RÉPÉTER nb_participants ??? FOIS
        numero_1 = ???
        numero_2 = ???
        print(numero_1, numero_2)
    ```

## 9) Banquet municipal

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=17) est de :

> Afficher, pour chaque position, le numéro de la personne qui s'y trouve une fois tous les changements faits.


??? tip "Indice"
    Modifier le code

    ```python
    nb_positions = int(input())
    nb_changements = int(input())
    numeros = TABLEAU de taille nb_positions

    RÉPÉTER nb_positions FOIS
        position = int(input())
        numeros DE ??? = position
    
    RÉPÉTER nb_changements FOIS
        position_1 = int(input())
        position_2 = int(input())
        ...
    
    RÉPÉTER nb_positions FOIS
        position = ???
        print(position)
    ```

## 10) Choix des emplacements

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=651&iOrder=18) est de :

> Inverser un tableau.

??? tip "Indice 1"
    Modifier le code

    ```python
    nb_emplacements = int(input())
    ??? = TABLEAU de taille nb_emplacements

    RÉPÉTER POUR CHAQUE i_emplacement
        i_marchand = int(input())
        ...

    RÉPÉTER POUR CHAQUE i_marchand
        i_emplacement = ???
        print(i_emplacement)
    ```

??? tip "Indice 2"
    
    1. Ne pas utiliser la variable `numero` ; elle ferait une confusion, elle utilisée pour les deux...

    2. Bien choisir le nom de variable pour le tableau ; ça simplifie la lecture et la compréhension.
