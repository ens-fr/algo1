# 📉 Statistiques 

Faire des statistiques élémentaires est un excellent moyen de s'entrainer avec les premiers algorithmes.

{{ python_carnet('scripts/statistiques/sujet.ipynb', module='scripts/statistiques/teste.py') }}

??? done "Corrigé"

    {{ python_carnet('scripts/statistiques/correction.ipynb', module='scripts/statistiques/teste.py') }}
