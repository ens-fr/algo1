"""Module de correction du carnet "Statistiques"

Auteur : Franck CHAMBON

"""

from random import randrange
from math import isclose, sqrt
sont_proches = isclose

MSG_TEST_OK = "Tests réussis 😀"

def _moyenne(population):
    return sum(population) / len(population)

def teste(fonction):

    nom_fonction = fonction.__name__

    if nom_fonction == 'effectif':
        effectif = fonction
        # petits tests
        assert effectif([42.12, 56.985, 0.021]) == 3, "Échec au test"
        assert effectif([]) == 0, "Échec au test"
        assert effectif([1, 1, 1, 2, 2]) == 5, "Échec au test"

        # gros test
        test_effectif = randrange(10**3)
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        assert effectif(test_population) == test_effectif, "Échec sur grand test"

        return MSG_TEST_OK

    elif nom_fonction == 'somme':
        somme = fonction
        # petits tests
        assert somme([]) == 0
        assert somme([42]) == 42
        assert somme([1, 2, 3, 4, 5]) == 1 + 2 + 3 + 4 + 5

        # gros test
        test_effectif = randrange(10**3)
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        test_somme = sum(test_population)
        assert somme(test_population) == test_somme, "Échec sur grand test"
        
        return MSG_TEST_OK

    elif nom_fonction == 'moyenne':
        moyenne = fonction
        # petits tests
        assert sont_proches(moyenne([42]), 42)
        assert sont_proches(moyenne([1, 2, 3, 4, 5]), (1 + 2 + 3 + 4 + 5) / 5)
        assert sont_proches(moyenne([321.97, 851.04, 761.87]), (321.97 + 851.04 + 761.87) / 3)

        # gros test
        test_effectif = randrange(10**3)
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        test_moyenne = sum(test_population) / len(test_population)
        assert sont_proches(moyenne(test_population), test_moyenne), "Échec sur grand test"

        return MSG_TEST_OK


    elif nom_fonction == 'variance':
        variance = fonction
        # petits tests
        assert sont_proches(variance([42]), 0.0)
        assert sont_proches(variance([1, 2, 3, 4, 5]), 2.0)
        assert sont_proches(variance([321.97, 851.04, 761.87]), 160459.4546 / 3)

        # gros test
        test_effectif = randrange(10**3)
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        test_moyenne = _moyenne(test_population)
        carre = lambda x: x*x
        test_variance = _moyenne(list(map(carre, [x - test_moyenne for x in test_population])))
        assert sont_proches(variance(test_population), test_variance), "Échec sur grand test"
        
        return MSG_TEST_OK

    elif nom_fonction == 'ecart_type':
        ecart_type = fonction
        # gros test
        test_effectif = randrange(10**3)
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        test_moyenne = _moyenne(test_population)
        carre = lambda x: x*x
        test_ecart_type = sqrt(_moyenne(list(map(carre, [x - test_moyenne for x in test_population]))))
        assert sont_proches(ecart_type(test_population), test_ecart_type), "Échec sur grand test"
        
        return MSG_TEST_OK

    elif nom_fonction == 'etendue':
        etendue = fonction
        # petits tests
        assert sont_proches(etendue([42]), 0.0)
        assert sont_proches(etendue([1, 2, 3, 4, 5]), 4.0)
        assert sont_proches(etendue([2, 1, 5, 4, 3]), 4.0)
        assert sont_proches(etendue([3, 4, 5, 1, 2]), 4.0)

        # gros test
        test_effectif = randrange(10**3)
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        test_etendue = max(test_population) - min(test_population)
        assert sont_proches(etendue(test_population), test_etendue), "Échec sur grand test"

        return MSG_TEST_OK

    elif nom_fonction == 'mediane':
        mediane = fonction
        # petits tests
        assert sont_proches(mediane([0, -5, 5]), 0.0)
        assert sont_proches(mediane([1, 2, 3, 4, 5]), 3.0)
        assert sont_proches(mediane([1, 2, 3, 4, 5, 6]), 3.5)
        assert sont_proches(mediane([-1, -2, -3, -4, -5]), -3.0)
        assert sont_proches(mediane([-4, 4, -2, 2, -1, 1]), 0.0)

        # gros tests
        test_effectif = randrange(500) * 2 + 1
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        test_population.sort()
        test_mediane = test_population[(test_effectif - 1)//2]
        assert sont_proches(mediane(test_population), test_mediane), "Échec sur grand test"

        test_effectif = randrange(500) * 2
        test_population = [randrange(10**9) for _ in range(test_effectif)]
        test_population.sort()
        test_mediane_inf = test_population[test_effectif // 2 - 1]
        test_mediane_sup = test_population[test_effectif // 2]
        test_mediane = (test_mediane_inf + test_mediane_sup) / 2
        assert sont_proches(mediane(test_population), test_mediane), "Échec sur grand test"

        return MSG_TEST_OK

