# 🧮 Entier

## Quatre premières opérations

> On évoquera la division dans la section suivante.

!!! tip "Avec Python, pour deux entiers"

    | Opération      |Opérateur|
    |:---------------|:-------:|
    | Addition       | `+`  |
    | Soustraction   | `-`  |
    | Multiplication | `*`  |
    | Puissance      | `**` |

!!! savoir "À savoir"
    - Les priorités usuelles sont respectées.
    - Les multiplications doivent être **explicites** :
        - On peut écrire `3*n + 1` pour $3n + 1$,
        - **mais pas** `3n + 1` qui provoquera une erreur.
    - On peut mettre des parenthèses `( )` pour forcer les priorités,
        - **mais pas** de crochets `[ ]` ou accolades `{ }` qui ont un autre rôle.
    - L'opérateur pour la puissance est `**`
    - Le calcul sur les entiers est en précision arbitraire :
        - La seule limite étant la mémoire de l'ordinateur.
        - On peut **travailler** avec des entiers qui ont des millions de chiffres,
        - mais **pas afficher** ceux qui sont trop grands.


## Exemples

```pycon
>>> 43 * 47
2021
>>> (45 - 2) * (45 + 2)
2021
>>> 23**2 + 197**2  # 23 au carré, plus 197 au carré
39338
>>> 97**2 + 173**2
39338
>>> 107**2 + 167**2
39338
>>> 113**2 + 163**2
39338
```

!!! cite "Humour _geek_"
    ![](images/xkcd-859.png){ title="Brains aside, I wonder how many poorly-written xkcd.com-parsing scripts will break on this title (or ;;&quot;&#39;&#39;{&lt;&lt;[&#39; this mouseover text.&quot;" alt="(" align=right width=350 }

    (Une parenthèse laissée ouverte  
    crée une tension irrésolue  
    qui vous suivra toute la journée.

    Source : [XKCD](https://xkcd.com/859/){ target=_blank }

## En savoir plus

- Les entiers sont stockés en mémoire sous forme binaire, il n'y a que deux chiffres `0` et `1` ; les calculs sont plus pratiques pour le processeur. Par exemple $42$ est stocké avec le code binaire `101010`.

- Pour l'affichage en décimal d'un entier, il y a une étape de conversion vers le système décimal (avec nos dix chiffres), et **ensuite** l'affichage des chiffres.

!!! warning "Conversion lente"
    L'affichage décimal serait rapide, **mais pas** la conversion binaire vers décimal, ainsi :

    - on peut **travailler** avec des nombres gigantesques,
    - mais **sans pouvoir les afficher**.

!!! tip "Entrer de grands nombres"
    En bref :

    - `100_000` est plus lisible que `100000`.
    - `100 000` provoque une erreur, il manque le tiret-bas.
    - `100 * 1000` était une ancienne méthode.

    Avec une version récente de Python, on peut utiliser le
     caractère `_` (tiret-bas) comme séparateur de milliers, millions, etc.

    - On recommande de le faire pour les nombres à partir de 5 chiffres.

    - Avec d'autres langages de programmation ou une ancienne version
     de Python, on peut écrire `60 * 1000 * 1000` par exemple.


## Exercice

- Dans un carnet, une cellule d'entrée (_input_) commence par `In [ ]:` ou `Entrée[ ]:`
- Entrer une suite d'opérations mathématiques dans la cellule ci-dessous.
    - Par exemple `1 + 2 * 3**4`
- Appuyer sur ++shift+return++ ; vous aurez le résultat (_output_), dans `Out[ ]:` ou `Sortie[ ]:`
- Il est aussi possible de cliquer sur :fontawesome-solid-step-forward: Exécuter
- Vous pouvez tester d'autres opérations.

{{ python_carnet('scripts/entier.ipynb') }}

!!! info "Carnet Basthon"
    [Basthon](https://basthon.fr/){ target=__blank rel='noopener' } est une autre façon d'utiliser Pyodide, et donc d'avoir Pytho dans votre navigateur sans aucune installation.

    Basthon sera utile pour créer des carnets, ou pour utiliser des modules puissants de Python.

    [Basthon respecte aussi le RGPD.](https://basthon.fr/about.html#privacy) { target=__blank rel='noopener' }
