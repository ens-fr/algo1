# 🧑🏼‍🏫 Nb Fermat - Exercice

## Définition

Les nombres de Fermat[^1] sont de la forme $F_n = 2^{2^n} + 1$, pour $n$ entier.

[^1]: :fontawesome-brands-wikipedia-w: [Les nombres de Fermat](https://fr.wikipedia.org/wiki/Nombre_de_Fermat){ target=_blank }

## Exemples

|$F_0$|$F_1$|$F_2$|$F_3$|$F_4$|
|:---:|:---:|:---:|:---:|:---:|
| $2^1+1=3$ | $2^2+1=5$ | $2^4+1=17$|$2^8+1=257$|$2^{16}+1=65537$|

!!! info "Information"
    - $F_0$, $F_1$, $F_2$, $F_3$, $F_4$ sont des nombres premiers.
    - Pierre de Fermat[^2] a conjecturé en 1640 que tous les nombres $F_n$ sont premiers.

[^2]: :fontawesome-brands-wikipedia-w: [Pierre de Fermat](https://fr.wikipedia.org/wiki/Pierre_de_Fermat){ target=_blank }

!!! tip "Exercice faisable avec une calculatrice"
    1. Calculer $F_5$.
    2. Vérifier que le reste dans la division de $F_5$ par $641$ est zéro.
    3. Donner le quotient de $F_5$ par $641$.
    4. **Remarque** : En 1732, le jeune Leonhard Euler, à qui Christian Goldbach avait signalé cette conjecture trois ans auparavant, la réfute : $F_5$ est divisible par $641$. Il ne dévoile la construction de sa preuve que quinze ans plus tard. Il y utilise une méthode similaire à celle qui avait permis à Fermat de factoriser les nombres de Mersenne $M_{23}$ et $M_{37}$.

!!! warning "Python devient très utile !"
    1. Calculer $F_6$, avec la console Basthon ci-dessous.
    2. Vérifier que le reste dans la division de $F_6$ par $274\,177$ est zéro.
    3. Donner le quotient de $F_6$ par $274\,177$.
    4. **Remarque** : En 1855, Thomas Clausen[^3] obtient avec ce quotient le plus grand nombre premier connu à cette époque.

[^3]: :fontawesome-brands-wikipedia-w: [Thomas Clausen](https://fr.wikipedia.org/wiki/Thomas_Clausen_(astronome)){ target=_blank }

!!! fail "Culture - une réelle difficulté"
    Au sujet des suivants[^4], on constate une difficulté très importante pour progresser.

    1. $F_7$ est divisible par $59\,649\,589\,127\,497\,217$. *Factorisation de $F_7$ obtenue en 1970.*
    2. $F_8$ à $F_{11}$ ont pu être entièrement factorisés. *Factorisation de $F_{11}$ obtenue en 1988.*
    3. En 2021, $F_{12}$ n'est toujours pas factorisé entièrement.

[^4]: _[Fermat factoring status](http://www.prothsearch.com/fermat.html){ target=_blank }_

## Exercice

1. Dans le script,
    - remplacer `...` par `2**( ... ) + 1`
    - et compléter
    - puis :fontawesome-solid-cogs: exécuter le script.
2. Dans la console, vérifier que :
    a. $F(4)$ est égal à $65\,537$.
    b. $F(5)$ est divisible par $641$.
    c. $F(6)$ est divisible par $274\,177$.
3. En déduire des facteurs de $F(5)$ et $F(6)$.
4. Afficher les 9 derniers chiffres de $F(12)$.

{{ IDEv('fermat') }}

??? done "Solution"
    - Dans le script,
        - on complète `#!python return 2**(2**n) + 1`
        - on lance le script.

    - Dans la console :
      
        ```pycon
        >>> F(4)
        65537
        >>> F(5) % 641
        0
        >>> F(6) % 274177
        0
        >>> F(5) // 641
        6700417
        >>> F(6) // 274177
        67280421310721
        >>> F(12) % 10**9
        154190337
        ```

!!! savoir "À savoir"
    - `#!python def` permet de **déf**-inir une fonction, ici avec un paramètre.
    - `#!pycon >>> F(5)` est un appel de fonction qui renvoie $F_5$ à la console, qui l'affiche.
    - **Attention**, ce n'est pas la fonction qui affiche $F_5$, la fonction **renvoie** $F_5$.
    - C'est la console qui **affiche**.
    - Nous le reverrons :wink:.

## Défi 💥

Quel est le 2021^e^ chiffre en partant des unités de $F_{13}$ ?

??? tip "Indice 1"
    Pour enlever les 20 derniers chiffres, on peut faire `F(13) // 10**20`

    Avec ce résultat, comment obtenir le dernier chiffre qui serait le 21^e^ chiffre ?

    Et pour le 2021^e^ ?

??? done "Solution 1"

    ```pycon
    >>> F(13) // (10**2020) % 10
    1
    ```

    **Explication méthode 1** : Si on voulait avoir le 4^e^ chiffre de $123\,456\,789$ en partant des unités, on diviserait par $10^3$, on obtiendrait $123\,456$, puis on prendrait le reste dans la division par $10$, pour obtenir $6$.

??? tip "Indice 2"
    Pour conserver les 21 derniers chiffres, on peut faire `F(13) % 10**21`

    Enlever les 20 derniers chiffres à ce résultat donne le 21^e^ chiffre de $F_{13}$.

    Et pour le 2021^e^ ?

??? done "Solution 2"

    ```pycon
    >>> F(13) % 10**2021 // 10**2020
    ```

    **Explication méthode 2** : Si on voulait avoir le 4^e^ chiffre de $123\,456\,789$ en partant des unités, on prendrait le modulo par $10^4$, on obtiendrait $6789$, puis on diviserait par $10^3$, pour obtenir $6$.
