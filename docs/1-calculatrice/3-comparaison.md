# 🔀 Comparaison

## À savoir

!!! tip "Comparaisons avec Python"
    | Comparaison               |Opérateur|
    |:--------------------------|:----:|
    | Strictement inférieur à   | `<`  |
    | Inférieur ou égal à       | `<=` |
    | Égal à                    | `==` |
    | Différent de              | `!=` |
    | Supérieur ou égal à       | `>=` |
    | Strictement supérieur à   | `>`  |

    Ces opérateurs entre deux entiers renvoient un **booléen** :

    - `True`, pour Vrai
    - `False`, pour Faux

## Exemples

```pycon
>>> (50 + 3)**2 == 50**2 + 2*50*3 + 3**2  # On teste (1)
True
>>> 2**10_000 <= 10**3000  # Une comparaison avec (2)
False
>>> 2**10_000 < 10**3100
True
```

1. une identité remarquable (a+b)² = a² +2ab + b²
2. des nombres très grands ; trop grands pour une calculatrice.


!!! info "Comprendre l'avant-dernière sortie"
    On sait que $2^{10} = 1024 > 1000 = 10^3$, donc

    $$2^{10000} = 2^{10×1000} = {\left(2^{10}\right)}^{1000} > {\left(10^3\right)}^{1000} = 10^{3000}$$

    Et la dernière sortie montre que l'on n'était pas loin.

!!! tip "Astuce : intervalle"
    Pour savoir si un nombre est dans un intervalle, on peut faire deux comparaisons enchainées.

    ```pycon
    >>> 10**3000 <= 2**10_000 < 10**3100
    True
    ```

    Ceci prouve que $2^{10\,000} \in [\![10^{3000} ; 10^{3100}[\![$

## Exercices

Vous pourrez utiliser le terminal ci-dessous pour faire les exercices.

!!! faq "Exercice 1"
    1. Trouver une puissance de $1337$ qui est supérieure à $10^{51}$
    2. Trouver la plus petite puissance de $1337$ qui soit supérieure à $10^{51}$.

!!! faq "Exercice 2"
    1. Trouver un entier $n$ tel que $n^{42}$ est supérieur à $19^{76}$
    2. Trouver le plus petit entier $n$ vérifiant cette propriété.

{{ python_carnet() }}

> Utiliser Python doit devenir un réflexe.
