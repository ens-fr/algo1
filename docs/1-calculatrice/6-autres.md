# 🛠️ Autres nombres

!!! tip "L'absence de résultat"
    `None` n'est pas un nombre. On l'utilisera pour signifier l'absence de résultat.

!!! info "Les fractions"
    Python permet aussi de travailler avec des fractions, (module `fractions`) et on retrouvera les mêmes opérateurs que pour les flottants. Nous le reverrons en partie 2.

    ```pycon
    >>> from fractions import Fraction
    >>> Fraction(3, 7) + Fraction(-1, 7) / Fraction(2, 3)
    Fraction(3, 14)
    >>>
    ```

!!! danger "Maths expertes"
    Pour ceux qui savent ce qu'est un nombre complexe, les mêmes opérateurs fonctionnent avec les nombres complexes. Si un opérande est complexe, alors l'autre est automatiquement converti avant calcul en complexe. Nous le reverrons en partie 3.

    ```pycon
    >>> (1 + 2j) / (1 - 2j)
    (-0.6+0.8j)
    >>>
    ```
