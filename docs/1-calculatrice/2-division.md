# ➗ Division entière et modulo

## Les opérateurs

!!! tip "Avec Python, pour deux entiers"

    | Opération       |Opérateur|
    |:----------------|:----:|
    | Quotient entier | `//` |
    | reste entier    | `%`  |

## Exemples

```pycon
>>> 1984 // 100
19
>> 1984 % 100
84
```

!!! warning  "Ne pas oublier de doubler le caractère `/`"
    Sinon, l'opérateur `/` renvoie un nombre en virgule flottante.

    ```python
    >>> 1984 / 100
    19.84
    ```

    Nous verrons plus tard les flottants.

## Exercices

{{ python_carnet('scripts/division.ipynb') }}



## Astuces utiles

!!! check "Derniers chiffres d'un grand nombre"
    Pour calculer les 5 derniers chiffres d'un nombre $n$ très grand. On peut faire

    - `n % 100_000`, ou bien
    - `n % 10**5`

    Exemple : 
    
    ```pycon
    >>> 2**2021 % 10**5
    37152
    ```
    Les derniers chiffres de $2^{2021}$ sont `37152`.


## En savoir plus

!!! cite "Notation"
    - L'opérateur `%` s'appelle `modulo`.
    - Il donne son nom au calcul modulaire.
    - En cryptographie, pour la sécurisation des données, on utilise massivement le calcul modulaire. Il faut s'entrainer à utiliser cet opérateur.
