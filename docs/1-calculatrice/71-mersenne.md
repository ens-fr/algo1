# 🧑🏻‍🎓 Nb Mersenne - Histoire

À la recherche de grands nombres premiers.

> L'objectif est de placer un contexte historique et de présenter les difficultés rencontrées. On montre comment les résoudre grâce à Python.

## Définition

Les nombres de Mersenne[^1] sont de la forme : $M_n = 2^n -1$, pour $n$ un entier non nul.

[^1]: :fontawesome-brands-wikipedia-w: [Les nombres de Mersenne premiers](https://fr.wikipedia.org/wiki/Nombre_de_Mersenne_premier){ target=_blank }

!!! example "Exemples"
    - $M_1 = 2^1 -1 = 1$, n'est pas premier, par définition
    - $M_2 = 2^2 -1 = 3$, est **premier**, 2 aussi
    - $M_3 = 2^3 -1 = 7$, est **premier**, 3 aussi
    - $M_4 = 2^4 -1 = 15$, n'est pas premier, $3\times5$
    - $M_5 = 2^5 -1 = 31$, est **premier**, 5 aussi
    - $M_6 = 2^6 -1 = 63$, n'est pas premier, $3\times3\times7$
    - $M_7 = 2^7 -1 = 127$, est **premier**, 7 aussi
    - $M_8 = 2^8 -1 = 255$, n'est pas premier, $3\times5\times17$
    - $M_9 = 2^9 -1 = 511$, n'est pas premier, $7\times73$
    - $M_{10} = 2^{10} -1 = 1023$, n'est pas premier, $3\times11\times13$
    - $M_{11} = 2^{11} -1 = 2047$, n'est pas premier, $23\times89$, **pourtant 11 est premier**
    - $M_{12} = 2^{12} -1 = 4095$, n'est pas premier, $3\times3\times5\times7\times13$

!!! tip "Plus grand nombre premier"
    Le plus grand nombre premier connu[^2] a souvent été un nombre de Mersenne.

[^2]: :fontawesome-brands-wikipedia-w: [Le plus grand nombre premier connu](https://fr.wikipedia.org/wiki/Plus_grand_nombre_premier_connu){ target=_blank }

## Histoire

### Au XVII^e^ siècle

![Par http://www.york.ac.uk/depts/maths/histstat/people/Abb.4 fromH Loeffel, Blaise Pascal, Basel: Birkhäuser 1987.DSB 9, 316-322., Domaine public, https://commons.wikimedia.org/w/index.php?curid=111592](images/Marin_Mersenne.jpg){ align=right width=200 target=_blank }

Au début du XVII^e^ siècle, Marin Mersenne[^3], moine de l'ordre des Minimes, pense et démontre peut-être que « Si $M_n$ est premier alors $n$ aussi ». La réciproque est fausse, comme le montre $n = 11$.

[^3]: :fontawesome-brands-wikipedia-w: [Marin Mersenne](https://fr.wikipedia.org/wiki/Marin_Mersenne){ target=_blank }

!!! danger "Difficultés"
    Marin Mersenne fournit aussi une liste des nombres premiers « de Mersenne » jusqu’à l'exposant 257. Cette liste contenait des erreurs ; il y a de vraies difficultés, comme nous le verrons par la suite.

### Au XVIII^e^ siècle

![Par Jakob Emanuel Handmann — 2011-12-22 (upload, according to EXIF data), Domaine public, https://commons.wikimedia.org/w/index.php?curid=1001511](images/Leonhard_Euler.jpg){ align=right width=300 target=_blank }

En 1772, Leonhard Euler[^4] prouve que $M_{31}$ est un nombre premier.

[^4]: :fontawesome-brands-wikipedia-w: [Leonhard Euler](https://fr.wikipedia.org/wiki/Leonhard_Euler){ target=_blank }

- Il lui a suffi de poser $372$ divisions pour cela.

!!! tip "Avec Python, aujourd'hui"
    On peut calculer $M_{31}$ ainsi :

    ```pycon
    >>> 2**31 - 1
    2147483647
    ```

!!! info "Record de 1772"
    Le nombre $2\,147\,483\,647$ est resté le plus grand nombre premier connu jusqu'en 1867.

### Au XIX^e^ siècle

![Par Zagel — http://gallica.bnf.fr/ark:/12148/bpt6k2135895/f33.item, Domaine public, https://commons.wikimedia.org/w/index.php?curid=45437813](images/Edouard_Lucas.jpg){align=right width=200 target=_blank }

Édouard Lucas[^5] a inventé un test de primalité,

[^5]: :fontawesome-brands-wikipedia-w: [Édouard Lucas](https://fr.wikipedia.org/wiki/%C3%89douard_Lucas){ target=_blank }

- qui lui a permis de prouver en 1876 que $M_{127}$ est premier.

!!! info "Record de 1876"
    Ce nombre a gardé le record jusqu'en 1951,

    - le début de l'ère moderne des ordinateurs.

### Au XX^e^ siècle

!!! info "Record de 1951"
    Ce n'est pas un nombre de Mersenne, c'est une exception, mais il en est dérivé. Nombre premier prouvé à l'aide d'un ordinateur.
    ```pycon
    >>> 180 * (2**127 - 1)**2 + 1
    5210644015679228794060694325390955853335898483908056458352183851018372555735221
    ```


![Par George Bergman — https://opc.mfo.de/detail?photo_id=5852, GFDL 1.2, https://commons.wikimedia.org/w/index.php?curid=18513042](images/Raphael_M._Robinson.jpg){align=right width=200 target=_blank }
En 1952, Raphael Robinson[^6] prouve que $M_{521}$ est premier à l'aide d'un ordinateur.

[^6]: :fontawesome-brands-wikipedia-w: [Raphael Robinson](https://fr.wikipedia.org/wiki/Raphael_Robinson){ target=_blank }

- Il utilise un ordinateur SWAC[^7].

[^7]: :fontawesome-brands-wikipedia-w: L'ordinateur [SWAC](https://fr.wikipedia.org/wiki/SWAC){ target=_blank }

!!! info "Records modernes"
    Depuis, le record est presque toujours détenu par un nombre de Mersenne.

![By National Institute of Standards and Technology - National Institute of Standards and Technology, Public Domain, https://commons.wikimedia.org/w/index.php?curid=31539951](images/SWAC.jpg){align=right width=200 target=_blank }

!!! tip "Affichage"
    Avec un émulateur en ligne comme Pyodide[^8], on peut afficher $M_{21\,701}$ qui a $6\,533$ chiffres.

    [^8]: :fontawesome-brands-python: Le noyau [Pyodide](https://pyodide.org/en/stable/){ target=_blank } apporte une version récente de Python via WebAssembly.

    Le dernier record du XX^e^ siècle, $M_{6\,972\,593}$ peut être affiché raisonnablement avec Python installé sur poste fixe, il a $2\,098\,960$ chiffres.

??? warning "Peut-on afficher les records récents ?"
    Python peut faire des calculs avec des entiers aussi grands que la mémoire de l'ordinateur le permet.

    - **Python peut travailler** avec des nombres qui ont des millions de chiffres.
    - **Python ne peut pas afficher** de tels nombres.
    - Ce n'est pas une histoire d'écran, c'est une histoire de conversion binaire vers décimal.

### Au XXI^e^ siècle

GIMPS[^9] est un projet de calcul partagé, fondé par George Woltman. Ce projet a permis de trouver les plus grands nombres premiers récents.

[^9]: :fontawesome-brands-wikipedia-w: [GIMPS](https://fr.wikipedia.org/wiki/Great_Internet_Mersenne_Prime_Search){ target=_blank } : _Great Internet Mersenne Prime Search_

- Les derniers records ont plus de 20 millions de chiffres et sont difficiles à afficher avec Python.

!!! danger "Reste modulaire"
    Même si on ne peut pas afficher un nombre très grand, on peut travailler avec et, par exemple, calculer un reste modulaire.

    Le calcul modulaire est omniprésent en cryptographie.



