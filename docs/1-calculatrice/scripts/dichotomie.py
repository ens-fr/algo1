def F(n):
    """
    Renvoie le nombre de Fermat F_n.
    """
    return 2**(2**n) + 1
    
