def nb_partitions(n):
    """Renvoie le nombre de partition de n
    comme une somme d'entiers,
    sans compter l'ordre.
    (Force brute)
    """
    def part(n, k):
        """Renvoie le nombre de partitions de n
        comme somme de k entiers,
        sans compter l'ordre.
        """
        if k == 1:
            return 1
        if k > n:
            return 0
        return part(n-1, k-1) + part(n-k, k)
    
    résultat = 0
    k = 0
    for _ in range(n):
        k = k + 1
        résultat = résultat + part(n, k)
    return résultat

n = 42
print(nb_partitions(n))
