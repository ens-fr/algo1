# 🧑🏻‍🏫 Nb Mersenne - Exercice

## Utilisation de la console

{{ IDEv() }}

{{ numworks() }}

!!! tip "Consignes"
    Calculer $M_{127}$, c'est-à-dire $2$ à la puissance $127$, moins 1

    1. Dans le terminal Python, entrer votre instruction
     à la suite de l'invite de commande `>>>`.
    
    2.  Sous la calculatrice, cliquer sur ++"⤡"++ pour passer en plein panneau.  
        Dans la calculatrice NumWorks,
    
        - appuyer deux fois sur ++"ᐅ"++,
        - puis ++"OK"++ pour entrer dans mode Python.
        - Faire défiler ++"ᐁ"++ jusqu'en bas pour avoir la console.
        - Remonter ++"ᐃ"++, et faire défiler avec ++"ᐅ"++
            le résultat qui est sur une seule ligne.

??? done "Solution"

    ```pycon
    >>> 2**127 - 1
    170141183460469231731687303715884105727
    ```

    ![NumWorks](images/screenshot.png)

!!! info "Record de 1876"
    Ce nombre a gardé le record du plus grand nombre premier connu jusqu'en 1951, le début de l'ère moderne des ordinateurs.
