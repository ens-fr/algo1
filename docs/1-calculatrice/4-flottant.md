# ♾️ Flottants

!!! warning "Les flottants"
    Les flottants [^1] (_**float**ing point numbers_) s'utilisent comme les nombres décimaux, **mais il y aura des précautions à prendre**.

    [^1]: :fontawesome-brands-wikipedia-w: Les flottants ; la norme [IEEE_754](https://fr.wikipedia.org/wiki/IEEE_754){ target=_blank }

!!! tip "Conseil"
    - Commencer à travailler avec les nombres entiers.
    - Lire au moins une fois ce cours.
    - Quand vous serez à l'aise avec les nombres entiers,
        - revenez et **relisez ce cours avec attention !**

## Les choses simples avec les flottants

!!! savoir "À savoir"
    - Le point `.` est le séparateur décimal.
    - Ces nombres sont stockés en binaire (et non en décimal),
        - avec une précision souvent meilleure qu'avec une calculatrice,
        - mais pas arbitraire non plus.
    - On peut entrer directement un nombre en écriture scientifique en utilisant la notation [^2] `e`
        - exemple : `-1.602e-19` pour $-1,602 \times 10^{-19}$, la charge en coulomb d'un électron.

    [^2]: :fontawesome-brands-wikipedia-w: La [notation e (_en_)](https://en.wikipedia.org/wiki/Scientific_notation#E-notation){ target=_blank }

!!! danger "Le nombre stocké est souvent différent du nombre entré"
    - Le nombre stocké sera l'approximation binaire du nombre décimal entré et sera souvent différent !
    - :warning: Le nombre en binaire sera encore un nombre décimal... mais pas forcément le même.

!!! example "Exemples simples expliqués"
    Cliquer sur la bulle pour avoir l'explication.

    ```python
    In [1]: from math import pi, sqrt  # Explication (1)

    In [2]: pi / 2  # Explication (2)
    Out[2]: 1.5707963267948966

    In [3]: 1.2**1000  # Explication (3)
    Out[3]: 1.5179100891722457e+79

    In [4]: 7.3 + 2  # Explication (4)
    Out[4]: 9.3

    In [5]: 21 / 3  # Explication (5)
    Out[5]: 7.0

    In [6]: 18 / 6.02e23  # Explication (6)
    Out[6]: 2.990033222591362e-23

    In [7]: sqrt(12.3)  # Explication (7)
    Out[7]: 3.5071355833500366
    ```

    1. Depuis (_from_) le module `math`, on importe :
        - une bonne approximation de π par un flottant,
        - une fonction racine carrée (_**sq**uare **r**oo**t**_) qui renvoie un flottant.
    2. Une approximation de π/2 donnée avec une quinzaine de chiffres décimaux significatifs.
        - La division[^3] entre flottants s'obtient avec l'opérateur `/`
    3. Un calcul d'une puissance d'un flottant.
        - Le résultat est donné en écriture scientifique, environ 1,5179×10⁷⁹
    4. On peut mélanger un entier et un flottant dans une opération.
        - **Détail important** : l'entier sera d'abord converti automatiquement en flottant avant le calcul.
    5. Si on utilise l'opérateur `/`,
        - les opérandes entiers sont automatiquement convertis en flottant avant le calcul ;
        - le résultat sera un flottant, même si la division entière a un reste nul.
    6. L'avant-dernier exemple calcule le volume moyen d'une molécule d'eau[^4] en ml, soit environ 30 Å^3^.
    7. On calcule la racine carrée d'un flottant.

    [^3]: :fontawesome-brands-wikipedia-w: La [division](https://fr.wikipedia.org/wiki/Division){ target=_blank }
    [^4]: :fontawesome-brands-wikipedia-w: La [constante d'Avogadro](https://en.wikipedia.org/wiki/Avogadro_constant){ target=_blank }

!!! abstract "Les opérateurs"
    On retrouve les opérateurs déjà vus avec les entiers, et on ajoute `/` pour la division non abrégée [^divlongue].

    `+` `-` `*` `**` `<` `<=` `==` `!=` `>=` `>` fonctionnent aussi avec les flottants.

    [^divlongue]: :fontawesome-brands-wikipedia-w: [La division non abrégée](https://fr.wikipedia.org/wiki/Division#Division_non_abr%C3%A9g%C3%A9e){ target=_blank }




## Points délicats

!!! warning "Attention"
    On retrouve comme sur de nombreuses calculatrices (et c'est normal) les points suivants :

    - Il existe des limites aux nombres flottants, avec
        - un plus petit flottant strictement positif,
        - un plus grand flottant positif,
        - et de même avec les négatifs.
    - Le nombre affiché ou entré n'est souvent pas égal au nombre représenté en machine.
    - Pour simplifier :
        - une quinzaine de chiffres significatifs, et
        - des exposants de la puissance de 10 entre -300 et +300, environ.

!!! example "Exemples expliqués"
    Cliquer sur la bulle pour avoir l'explication.

    ```python
    In [1]: 0.5**1000  # cette opération renvoie (1)
    Out[1]: 9.332636185032189e-302

    In [2]: 0.5**1100  # cette opération renvoie (2)
    Out[2]: 0.0

    In [3]: 2.0**1000  # cette opération renvoie (3)
    Out[3]: 1.0715086071862673e+301

    In [4]: 2.0**1100  # cette opération renvoie (4)
    Traceback (most recent call last):

      File "<stdin>", line 1, in <module>

    OverflowError: (34, 'Numerical result out of range')
    ```

    1. un résultat très petit, qui se rapproche du plus petit flottant strictement positif.
    2. un résultat, tellement petit, qu'il est arrondi à exactement zéro, en flottant.
    3. un résultat très grand, écrit en écriture scientifique.
    4. une erreur, le résultat étant trop grand.
      

    :warning: Notons que `2**1100` ne provoquerait **pas d'erreur** ; c'est un **entier** qui pourrait être bien plus grand encore sans perdre de précision, tant qu'il y a de la mémoire disponible.

### Stockage interne, souvent une approximation

!!! danger "Exemple à méditer"

    ```python
    In [1]: 0.1 + 0.2 == 0.3  # C'est choquant, mais (1)
    Out[1]: False
    ```

    1. **c'est Faux**

    Explications :

    - $0.1$ est approché en machine par un nombre qui n'est pas exactement égal à $0.1$[^5], mais par un nombre écrit en binaire, au plus proche. De même pour $0.2$ et $0.3$.
    - Le test d'égalité est réalisé sur les nombres binaires, pas sur les nombres affichés en décimal !
    - Une calculatrice a normalement le **même comportement**, sauf bug.

    [^5]: L'approximation de $0.1$ est stockée avec la valeur $\dfrac{3602879701896397}{2^{55}} =\dfrac{3602879701896397}{36028797018963968} = 0,\!1000000000000000055511151231257827021181583404541015625$ qui est lui aussi un décimal, mais surtout un quotient d'entier par une puissance de deux.

!!! savoir "Conclusion, à retenir"
    1. On ne doit **jamais** faire de test d'égalité entre flottants !
        - Par exemple, pour savoir si un triangle à côtés flottants $(a < b < c)$ est rectangle,
            - **on ne fera pas** le test `a*a + b*b == c*c`.
    2. Il faut apprendre à faire des arrondis à la précision que l'on souhaite.
        - Nous allons donc découvrir, entre autres, les fonctions natives `round` et `abs`.


### Solution simple

Voici un autre exemple à méditer

```pycon
>>> 1.23 == 12.3 / 10  # on a de la chance ici
True
>>> 1.37 == 13.7 / 10  # mais voilà ce qui arrive souvent
False
```

Pour savoir si deux flottants $x$ et $y$ sont **presque** égaux, une solution simple est de calculer la différence et de vérifier qu'elle est proche de zéro.

```pycon
>>> x = 1.23
>>> y = 12.3 / 10
>>> -10**-6 < x - y < 10**-6
True
>>> x = 1.37
>>> y = 13.7 / 10
>>> -10**-6 < x - y < 10**-6
True
```

On peut faire plus simple encore avec la fonction valeur absolue.

```pycon
>>> x = 1.23
>>> y = 12.3 / 10
>>> abs(x - y) < 10**-6
True
>>> x = 1.37
>>> y = 13.7 / 10
>>> abs(x - y) < 10**-6
True
```

## Exercice

!!! faq "Triangle rectangle"
    1. En utilisant Python, vérifier si le triangle $ABC$ est rectangle sachant que
      
        - $AB = 6.5~\text{cm}$
        - $BC = 7.2~\text{cm}$
        - $CA = 9.7~\text{cm}$
      
    2. Et si les longueurs étaient exprimées en millimètre ?

??? done "Réponse"
    1. Oui, ce triangle est rectangle en $B$.
        
        $CA$ est le plus grand des côtés, et on a :

        === "✅ Bonne méthode"
            ```pycon
            >>> AB = 6.5
            >>> BC = 7.2
            >>> CA = 9.7
            >>> abs(AB**2 + BC**2 - CA**2) < 10**-6
            True
            ```
        === "❌ Mauvaise méthode"
            ```pycon
            >>> AB = 6.5
            >>> BC = 7.2
            >>> CA = 9.7
            >>> AB**2 + BC**2 == CA**2
            False
            ```
      
    2. Avec les nombres entiers, c'est plus simple
      
        ```pycon
        >>> AB = 65
        >>> BC = 72
        >>> CA = 97
        >>> AB**2 + BC**2 == CA**2
        True
        ```

## À venir

- Dans cette section :
    - Travail avec les booléens.
    - Un mot sur les autres types numériques.
- Dans la section suivante :
    - Comment utiliser des variables.
    - Découvrir de nouvelles fonctions.
- Et ensuite ?
    - Créer nos propres fonctions.
    - Créer nos propres programmes.
    - ...
