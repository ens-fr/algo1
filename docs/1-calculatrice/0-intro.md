# Sommaire

Python est une calculatrice avancée.

- Les entiers (**int**, pour _integer_) sont aussi grands que la mémoire le permet. On découvre les premières opérations.
- Pour la division, il y aura plusieurs choses à savoir...
- Les nombres à virgule sont appelés les flottants (**float**, pour _floating point number_).
- Python peut aussi travailler avec d'autres nombres comme les fractions ou les nombres complexes.

---

Vous pouvez essayer d'utiliser cette calculatrice.

1. Entrer une expression à calculer.
2. Pour la puissance, on utilise `**`
2. Il est possible de calculer avec de très grands entiers !
    - L'affichage sera tronqué au milieu, s'il est trop gros.
4. Tous les calculs s'effectuent sur **votre** machine.
5. RGPD respecté.

{{ terminal() }}

